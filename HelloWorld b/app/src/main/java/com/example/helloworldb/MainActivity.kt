package com.example.helloworldb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.helloworldb.databinding.ActivityMainBinding

//import kotlinx.android.synthetic.main.activity_main.*

/*
first way:
the next two sentences are the old way to access and
        change a variable of the ui.
val myTV = findViewById<TextView>(R.id.myTV)
myTV.text = "Hello my lovely Students!"

second way (deprecated).
it requires to add a deprecated line to the
build.gradle (Module: <project name>) file:
id 'kotlin-android-extensions' //removed in latest versions.
and import:
    import kotlinx.android.synthetic.main.activity_main.*

myTV.text = "Hello my lovely Students, second option!"

third way (to use): remove the import and the additional
line in the gradle file you did in the second option.
it's necessary to add code to the build.gradle
(Module: <project name>) file:
    buildFeatures {
        viewBinding true
    }
 and import:
    import com.example.helloworld.databinding.ActivityMainBinding
 and add:
    private lateinit var binding: ActivityMainBinding
    binding = ActivityMainBinding.inflate(getLayoutInflater())
 and change the setContentView() parameters:
    setContentView(binding.root)
 */

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(getLayoutInflater())
        //setContentView(R.layout.activity_main)
        setContentView(binding.root)

        //val myTV = findViewById<TextView>(R.id.myTV)
        //myTV.text = "Hi Orlando option 2!"
        binding.myTV.text = "Hi Orlando option 3!"
    }
}