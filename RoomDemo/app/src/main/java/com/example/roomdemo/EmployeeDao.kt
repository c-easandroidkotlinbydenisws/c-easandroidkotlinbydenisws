package com.example.roomdemo

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

//create the dao interface.
@Dao
interface EmployeeDao {
    // create a suspend insert function for saving an entry.
    @Insert
    suspend fun insert(employeeEntity: EmployeeEntity)

    // create a suspend update function for updating an existing entry
    @Update
    suspend fun update(employeeEntity: EmployeeEntity)

    // create a suspend delete function for deleting an existing entry
    @Delete
    suspend fun delete(employeeEntity: EmployeeEntity)
    // create a function to read all employee, this returns a Flow
    @Query("Select * from `employee-table`")
    fun fetchAllEmployee(): Flow<List<EmployeeEntity>>

    // create a function to read one employee, this returns a Flow
    @Query("Select * from `employee-table` where id=:id")
    fun fetchEmployeeById(id:Int):Flow<EmployeeEntity>
}