package com.example.roomdemo

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

//creating a Data Model Class and using as entity
@Entity(tableName = "employee-table") //it will be the name of the table in the db.
data class EmployeeEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int=0,
    val name: String="",
    @ColumnInfo(name = "email-id")//this will the internal name. for qry I use email.
    val email: String=""
)
