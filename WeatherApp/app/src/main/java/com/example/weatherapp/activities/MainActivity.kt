package com.example.weatherapp.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.example.weatherapp.databinding.ActivityMainBinding
import android.provider.Settings
import android.util.Log
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import androidx.appcompat.app.AlertDialog
import android.net.Uri
import android.location.Location
import android.view.Menu
import android.view.MenuItem
import com.example.weatherapp.R
import com.example.weatherapp.models.WeatherResponse
import com.example.weatherapp.network.WeatherService
import com.example.weatherapp.utils.Constants
import com.example.weatherapp.utils.toast
import com.google.android.gms.location.*
import com.google.gson.Gson
import retrofit.*
import java.text.SimpleDateFormat
import java.util.Date
import java.util.TimeZone

// After creating a project as we are developing a weather app kindly visit the link below.
// OpenWeather Link : https://openweathermap.org/api

/**
 * The useful link or some more explanation for this app you can checkout this link :
 * https://medium.com/@sasude9/basic-android-weather-app-6a7c0855caf4
 */
class MainActivity : AppCompatActivity() {
    private var binding: ActivityMainBinding? = null
    // Add a variable for FusedLocationProviderClient.
    // A fused location client variable which is further used to get the user's current location
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    // Create a global variable for ProgressDialog.
    // A global variable for the Progress Dialog
    private var mProgressDialog: Dialog? = null
    // Add a variable for SharedPreferences
    // A global variable for the SharedPreferences
    private lateinit var mSharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)
//        toast(R.string.any_string)

        // Initialize the fusedLocationProviderClient variable.
        // Initialize the Fused location variable
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        // Initialize the SharedPreferences variable.
        // Initialize the SharedPreferences variable
        mSharedPreferences = getSharedPreferences(Constants.PREFERENCE_NAME, Context.MODE_PRIVATE)

        // Call the UI method to populate the data in
        //  the UI which are already stored in sharedPreferences earlier.
        //  At first run it will be blank.
        setupUI()

        // Check here whether GPS is ON or OFF using the method which we have created
        if (!isLocationEnabled()) {
            toast(R.string.your_location_provider_is_turned_off)

            // This will redirect you to settings from where you need to turn on the location provider.
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(intent)
        } else {
//            toast(R.string.your_location_provider_is_already_on) //used for testing before the following code.
            // Asking the location permission on runtime.
            Dexter.withActivity(this)
                .withPermissions(
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION
                )
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted()) {
                            // Call the location request function here.
                            requestLocationData()
                        }

                        if (report.isAnyPermissionPermanentlyDenied) {
                            toast(R.string.you_have_denied_location_permission)
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: MutableList<PermissionRequest>?,
                        token: PermissionToken?
                    ) {
                        showRationalDialogForPermissions()
                    }
                }).onSameThread()
                .check()
        }
    }

    // Now add the override methods to load the menu file and perform the selection on item click.
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            // Now finally, make an api call on item selection.
            R.id.action_refresh -> {
                requestLocationData()
                // Call the api calling function here.
//                getLocationWeatherDetails(latitude, longitude)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    // Check whether the GPS is ON or OFF
    /**
     * A function which is used to verify that the location or GPS is enabled or not.
     */
    private fun isLocationEnabled(): Boolean {

        // This provides access to the system location services.
        val locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                            locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }

    // A alert dialog for denied permissions and if needed to allow it from the settings app info.
    /**
     * A function used to show the alert dialog when the permissions are denied and need to allow it from settings app info.
     */
    private fun showRationalDialogForPermissions() {
        AlertDialog.Builder(this)
            .setMessage("It Looks like you have turned off permissions required for this feature. It can be enabled under Application Settings")
            .setPositiveButton(
                "GO TO SETTINGS"
            ) { _, _ ->
                try {
                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    val uri = Uri.fromParts("package", packageName, null)
                    intent.data = uri
                    startActivity(intent)
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
            .setNegativeButton("Cancel") { dialog,
                                           _ ->
                dialog.dismiss()
            }.show()
    }

    // Add a function to get the location of the device using the fusedLocationProviderClient.
    /**
     * A function to request the current location. Using the fused location provider client.
     */
    @SuppressLint("MissingPermission")
    private fun requestLocationData() {

        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    // Register a request location callback to get the location.
    /**
     * A location callback object of fused location provider client where we will get the current location details.
     */
    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation!!
            val latitude = mLastLocation.latitude
            Log.i("Current Latitude", "$latitude")

            val longitude = mLastLocation.longitude
            Log.i("Current Longitude", "$longitude")

            // Call the api calling function here.
            getLocationWeatherDetails(latitude, longitude)
        }
    }

    // Create a function to make an api call using Retrofit Network Library.
    /**
     * Function is used to get the weather details of the current location based on the latitude longitude
     */
    private fun getLocationWeatherDetails(latitude: Double, longitude: Double){
        // Here we will check whether the internet
        //  connection is available or not using the method which
        //  we have created in the Constants object.)
        if (Constants.isNetworkAvailable(this@MainActivity)) {
//            toast(R.string.you_have_connected_to_the_internet)

            // Make an api call using retrofit.
            /**
             * Add the built-in converter factory first. This prevents overriding its
             * behavior but also ensures correct behavior when using converters that consume all types.
             */
            val retrofit: Retrofit = Retrofit.Builder()
                // API base URL.
                                    .baseUrl(Constants.BASE_URL)
                /** Add converter factory for serialization and deserialization of objects. */
                /**
                 * Create an instance using a default {@link Gson} instance for conversion. Encoding to JSON and
                 * decoding from JSON (when no charset is specified by a header) will use UTF-8.
                 */
                                    .addConverterFactory(GsonConverterFactory.create())
                /** Create the Retrofit instances. */
                                    .build()

            // Further step for API call
            /**
             * Here we map the service interface in which we declares the end point and the API type
             *i.e GET, POST and so on along with the request parameter which are required.
             */
            val service: WeatherService =
                retrofit.create<WeatherService>(WeatherService::class.java)

            /** An invocation of a Retrofit method that sends a request to a web-server and returns a response.
             * Here we pass the required param in the service
             */
            val listCall: Call<WeatherResponse> = service.getWeather(
                latitude, longitude, Constants.METRIC_UNIT, Constants.APP_ID
            )

            // Show the progress dialog
            showCustomProgressDialog() // Used to show the progress dialog

            // Callback methods are executed using the Retrofit callback executor.
            //this code runs in the background.
            listCall.enqueue(object : Callback<WeatherResponse> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(response: Response<WeatherResponse>,
                                        retrofit: Retrofit) {
                            // Check weather the response is success or not.
                            if (response.isSuccess) {
                                // Hide the progress dialog
                                hideProgressDialog() // Hides the progress dialog

                                /** The de-serialized response body of a successful response. */
                                val weatherList: WeatherResponse = response.body()
                                Log.i("Response Result", "$weatherList")

                                // Here we convert the response object to string and store the string in the SharedPreference.)
                                // Here we have converted the model class in to Json String to store it in the SharedPreferences.
                                val weatherResponseJsonString = Gson().toJson(weatherList)
                                // Save the converted string to shared preferences
                                val editor = mSharedPreferences.edit()
                                editor.putString(Constants.WEATHER_RESPONSE_DATA, weatherResponseJsonString)
                                editor.apply()

                                // Call the setup UI method here and pass the response object as a parameter to it to get the individual values.
                                // Remove the weather detail object as we will be getting
                                //  the object in form of a string in the setup UI method.
                                //  setupUI(weatherList)
                                setupUI()
                            } else {
                                // If the response is not success then we check the response code.
                                val sc = response.code()
                                when (sc) {
                                    400 -> {
                                        Log.e("Error 400", "Bad Request")
                                    }
                                    404 -> {
                                        Log.e("Error 404", "Not Found")
                                    }
                                    else -> {
                                        Log.e("Error", "Generic Error")
                                    }
                                }
                            }
                }

                override fun onFailure(t: Throwable) {
                    // Hide the progress dialog
                    hideProgressDialog() // Hides the progress dialog
                    Log.e("Errorrrrr", t.message.toString())
                }
            })

        } else {
            toast(R.string.no_internet_connection_available)
        }
        // END
    }

    // Create a functions for SHOW and HIDE progress dialog.
    /**
     * Method is used to show the Custom Progress Dialog.
     */
    private fun showCustomProgressDialog() {
        mProgressDialog = Dialog(this)

        /*Set the screen content from a layout resource.
        The resource will be inflated, adding all top-level views to the screen.*/
        mProgressDialog!!.setContentView(R.layout.dialog_custom_progress)

        //Start the dialog and display it on screen.
        mProgressDialog!!.show()
    }

    /**
     * This function is used to dismiss the progress dialog if it is visible to user.
     */
    private fun hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog!!.dismiss()
        }
    }

    // We have set the values to the UI and also added some required methods for Unit and Time below.
    /**
     * Function is used to set the result in the UI elements.
     */
//    private fun setupUI(weatherList: WeatherResponse) {//before sharedPreferences
    private fun setupUI() {
        // Here we have got the latest stored response from the SharedPreference and converted back to the data model object.
        val weatherResponseJsonString =
            mSharedPreferences.getString(Constants.WEATHER_RESPONSE_DATA, "")
        if (!weatherResponseJsonString.isNullOrEmpty()) {
            val weatherList =
                Gson().fromJson(weatherResponseJsonString, WeatherResponse::class.java)
            // For loop to get the required data. And all are populated in the UI.
            for (z in weatherList.weather.indices) {
                Log.i("weather name", weatherList.weather[z].main)

                binding?.tvMain?.text = weatherList.weather[z].main
                binding?.tvMainDescription?.text = weatherList.weather[z].description
                binding?.tvTemp?.text =
                    weatherList.main.temp.toString() + getUnit(application.resources.configuration.locales.toString())
                binding?.tvHumidity?.text = weatherList.main.humidity.toString() + " per cent"
                binding?.tvMin?.text = weatherList.main.temp_min.toString() + " min"
                binding?.tvMax?.text = weatherList.main.temp_max.toString() + " max"
                binding?.tvSpeed?.text = weatherList.wind.speed.toString()
                binding?.tvName?.text = weatherList.name
                binding?.tvCountry?.text = weatherList.sys.country
                binding?.tvSunriseTime?.text = unixTime(weatherList.sys.sunrise.toLong())
                binding?.tvSunsetTime?.text = unixTime(weatherList.sys.sunset.toLong())

                // Here we update the main icon
                when (weatherList.weather[z].icon) {
                    "01d" -> binding?.ivMain?.setImageResource(R.drawable.sunny)
                    "02d" -> binding?.ivMain?.setImageResource(R.drawable.cloud)
                    "03d" -> binding?.ivMain?.setImageResource(R.drawable.cloud)
                    "04d" -> binding?.ivMain?.setImageResource(R.drawable.cloud)
                    "04n" -> binding?.ivMain?.setImageResource(R.drawable.cloud)
                    "10d" -> binding?.ivMain?.setImageResource(R.drawable.rain)
                    "11d" -> binding?.ivMain?.setImageResource(R.drawable.storm)
                    "13d" -> binding?.ivMain?.setImageResource(R.drawable.snowflake)
                    "01n" -> binding?.ivMain?.setImageResource(R.drawable.cloud)
                    "02n" -> binding?.ivMain?.setImageResource(R.drawable.cloud)
                    "03n" -> binding?.ivMain?.setImageResource(R.drawable.cloud)
                    "10n" -> binding?.ivMain?.setImageResource(R.drawable.cloud)
                    "11n" -> binding?.ivMain?.setImageResource(R.drawable.rain)
                    "13n" -> binding?.ivMain?.setImageResource(R.drawable.snowflake)
                }
            }
        }
    }

    /**
     * Function is used to get the temperature unit value.
     */
    private fun getUnit(value: String): String? {
        Log.i("unitttttt", value)
        var value = "°C"
        if ("US" == value || "LR" == value || "MM" == value) {
            value = "°F"
        }
        return value
    }

    /**
     * The function is used to get the formatted time based on the Format and the LOCALE we pass to it.
     */
    private fun unixTime(timex: Long): String? {
        val date = Date(timex * 1000L)
        @SuppressLint("SimpleDateFormat") val sdf =
            SimpleDateFormat("hh:mm")
        sdf.timeZone = TimeZone.getDefault()
        return sdf.format(date)
    }

    override fun onDestroy() {
        super.onDestroy()
        //TO avoid memory leak we disallow the binding once the activity is destroyed
        binding = null
    }
}