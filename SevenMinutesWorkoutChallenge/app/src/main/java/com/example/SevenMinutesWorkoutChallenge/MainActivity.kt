package com.example.SevenMinutesWorkoutChallenge

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.SevenMinutesWorkoutChallenge.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    //The binding is a name just like the name of the layout with Binding attached.
    //We create a variable for it and assign to null.
    private var binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //We inflate the late file by calling inflate on the Binding name
        binding = ActivityMainBinding.inflate(layoutInflater)
        //Then replace the setContentView parameter with binding?.root
        setContentView(binding?.root)

//        val fLStartButton: FrameLayout = findViewById(R.id.flStart)
        binding?.flStart?.setOnClickListener {
//            toast(R.string.start_the_exercise_button)
            val intent = Intent(this,ExerciseActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        //TO avoid memory leak we disallow the binding once the activity is destroyed
        binding = null
    }
}