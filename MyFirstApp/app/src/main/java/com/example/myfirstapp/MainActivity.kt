package com.example.myfirstapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // get reference to textview
        val btnClickMe = findViewById<Button>(R.id.mybutton)
        val tvMyTextView = findViewById<TextView>(R.id.textView)
        var timesClicked = 0
        btnClickMe.setOnClickListener {
//            btnClickMe.text = "HaHa you clicked me!"
//            tvMyTextView.text = "HaHa you clicked me!"
            timesClicked += 1
            tvMyTextView.text = timesClicked.toString()
            Toast.makeText(this@MainActivity, "Hi Orlando!", Toast.LENGTH_SHORT).show()
        }
    }
}