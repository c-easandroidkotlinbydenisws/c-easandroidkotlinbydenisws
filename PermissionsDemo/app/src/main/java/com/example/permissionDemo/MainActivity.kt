package com.example.permissionDemo

import android.os.Build
import android.os.Bundle
import android.widget.Button
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.example.myquizapp.toast
import android.Manifest
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    private val cameraResultLauncher: ActivityResultLauncher<String> =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()){
                isGranted ->
            if(isGranted){
                toast(R.string.permission_granted_for_camera)
            }else{
                toast(R.string.permission_denied_for_camera)
            }
        }
    //This time we create the Activity result launcher of type Array<String>
    private val cameraAndLocationResultLauncher: ActivityResultLauncher<Array<String>> =
        registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()){
                permissions->
            /**
            Here it returns a Map of permission name as key with boolean as value
            We loop through the map to get the value we need which is the boolean
            value
             */
            permissions.entries.forEach {
                val permissionName = it.key
                //if it is granted then we show its granted
                val isGranted = it.value
                if (isGranted) {
                    //check the permission name and perform the specific operation
                    if ( permissionName == Manifest.permission.ACCESS_FINE_LOCATION) {
                        toast(R.string.permission_granted_for_location)
                    }else{
                        //check the permission name and perform the specific operation
                        toast(R.string.permission_granted_for_camera)
                    }
                } else {
                    if ( permissionName == Manifest.permission.ACCESS_FINE_LOCATION) {
                        toast(R.string.permission_denied_for_location)
                    }else{
                        toast(R.string.permission_denied_for_camera)
                    }
                }
            }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnCameraPermission: Button = findViewById(R.id.btnCameraPermission)
        btnCameraPermission.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && shouldShowRequestPermissionRationale(
                    Manifest.permission.CAMERA
                )
            ) {
                showRationaleDialog(" Permission Demo requires camera access",
                    "Camera cannot be used because Camera access is denied")
            } else {
                // You can directly ask for the permission.
                // The registered ActivityResultCallback gets the result of this request.
                cameraAndLocationResultLauncher.launch(
                    arrayOf(Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                )
            }
        }
    }
        /**
         * Shows rationale dialog for displaying why the app needs permission
         * Only shown if the user has denied the permission request previously
         */
        private fun showRationaleDialog(title: String, message: String) {
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
            builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Cancel") { dialog, _ ->
                    dialog.dismiss()
                }
            builder.create().show()
        }
}