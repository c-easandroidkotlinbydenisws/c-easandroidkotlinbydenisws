package com.example.dobcalc

import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {
    private var tvSelectedDate: TextView? = null
    private var tvAgeInDays: TextView? = null

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnDatePicker : Button = findViewById(R.id.btnDatePicker)
        tvSelectedDate = findViewById(R.id.tvSelectedDate)
        tvAgeInDays = findViewById((R.id.tvAgeInDays))

        btnDatePicker.setOnClickListener{
            clickDatePicker()
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun clickDatePicker(){
        val myCalendar = Calendar.getInstance()
        val year = myCalendar.get(Calendar.YEAR)
        val month = myCalendar.get(Calendar.MONTH)
        val day = myCalendar.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(this,
                                 DatePickerDialog.OnDateSetListener { _, selectedYear, //the dash means an unused parameter.
                                                                      selectedMonth,
                                                                      selectedDayOfMonth ->
                                     Toast.makeText(this, "Year was $selectedYear, " +
                                                                    "month was ${selectedMonth + 1}, " +
                                                                    "day of month was $selectedDayOfMonth",
                                                                    Toast.LENGTH_LONG).show()

                                     //all variables ended in 2 are for calculation in days and in us format.
                                     val tempSelectedDate = "$selectedDayOfMonth/${selectedMonth + 1}/$selectedYear"
                                     val tempSelectedDate2 = "${selectedMonth + 1}/$selectedDayOfMonth/$selectedYear"
                                     tvSelectedDate?.text = tempSelectedDate //.text is equivalent to .setText(tempSelectedDate)
                                     val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
                                     val sdf2 = SimpleDateFormat("MM/dd/yyyy")

                                     val theDate = sdf.parse(tempSelectedDate) //converts the selected date in a computational format.
                                     val theDate2 = sdf2.parse(tempSelectedDate2)

                                     theDate?.let{//if theDate is not null do...
                                         val selectedDateInMinutes = theDate.time / 60000 //.time is equivalent to .getTime()
                                         val selectedDateInDays = theDate2.time / 86400000
                                         val currentDate = sdf.parse(sdf.format(System.currentTimeMillis()))
                                         val currentDate2 = sdf2.parse(sdf2.format(System.currentTimeMillis()))
                                         currentDate?.let{ //if currentDate is not null do...
                                             val currentDateInMinutes = currentDate.time / 60000
                                             val currentDateInDays = currentDate2.time / 86400000
                                             val differenceInMinutes = currentDateInMinutes - selectedDateInMinutes
                                             val differenceInDays = currentDateInDays - selectedDateInDays
//                                             tvAgeInDays?.text = differenceInMinutes.toString()
                                             tvAgeInDays?.text = differenceInDays.toString()
                                         }//end of the outer let function.
                                     }//end of the inner let function.
                                 }, //end of the listener (the second parameter of the DatePickerDialog).
                                 year,
                                 month,
                                 day) //the selected date comes into selectedYear, selectedMonth + 1, and selectedDayOfMonth.
        dpd.datePicker.maxDate = System.currentTimeMillis() - 86400000
        dpd.show()
    }
}