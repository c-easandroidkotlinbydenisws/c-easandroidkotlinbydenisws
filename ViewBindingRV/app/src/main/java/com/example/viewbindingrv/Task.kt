package com.example.viewbindingrv
//This Data Class is required for the RecyclerView Adapter to bind each of view items
//with their Data Class values

data class Task (val title: String, val timestamp: String )