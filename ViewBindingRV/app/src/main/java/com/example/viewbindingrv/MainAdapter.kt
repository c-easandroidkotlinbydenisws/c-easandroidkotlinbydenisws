package com.example.viewbindingrv

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.viewbindingrv.databinding.RecyclerviewItemBinding

//This adapter is used to connect RecyclerView items to their values in created data source list
class MainAdapter(val taskList:List<Task>): RecyclerView.Adapter<MainAdapter.MainViewHolder>(){

    /**
     * A ViewHolder describes an item view and metadata about its place within the RecyclerView.
     * A binding variable is created in its constructor for the item layout
     */
    inner class MainViewHolder(val itemBinding: RecyclerviewItemBinding)
        :RecyclerView.ViewHolder(itemBinding.root){
        fun bindItem(task:Task){
            itemBinding.tvTitle.text = task.title
            itemBinding.tvTime.text = task.timestamp
        }
    }

    //Next three functions are essential for RecyclerView.Adapter which MainAdapter inherits from,
    // and must be overridden for it to know how to properly function

    //What should happen once the RecyclerView is created
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        //Inflates the item views which is designed in xml layout file
        return MainViewHolder(RecyclerviewItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false))
    }

    //Bind each List item to the View
    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val task = taskList[position]
        holder.bindItem(task)
    }

    //Gets the number of items in the list
    override fun getItemCount(): Int {
        return taskList.size
    }
}