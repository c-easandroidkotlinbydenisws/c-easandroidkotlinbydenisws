package com.example.viewbindingrv

//This is a datasource to fill up the RecyclerView, Adapter uses it to get the data

object TaskList {
    val taskList = listOf<Task>(
        Task("Take a Walk", "7:00 am"),
        Task("Eat Breakfast", "8:00 am"),
        Task("Attend a meeting", "9:00 am"),
        Task("Read a Book", "12:00 pm"),
        Task("Eat Lunch", "2:00 pm"),
        Task("Buy Groceries", "3:00 pm"),
        Task("Take a Walk", "5:00 pm"),
        Task("Do a Yoga", "6:00 pm"),
        Task("Eat Dinner", "7:00 pm"),
        Task("Read a Book", "8:00 pm"),
        Task("Have a Shower", "9:00 pm"),
        Task("Go to Bed", "10:00 pm")
    )
}