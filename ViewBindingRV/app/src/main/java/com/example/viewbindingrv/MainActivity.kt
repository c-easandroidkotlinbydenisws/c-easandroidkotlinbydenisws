package com.example.viewbindingrv

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.viewbindingrv.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    //Setup the Main Activity viewbinding to use its xml layout objects
    private var binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Instantiate the MainActivity viewbinding object by filling it up with xml layout objects/items
        binding = ActivityMainBinding.inflate(layoutInflater)
        //Set the content of Main Activity screen using the viewbinding
        setContentView(binding?.root)

//        toast(R.string.any_string)

        //Setup the adapter to bind RecyclerView layout and the RecyclerView datasource items
        val adapter = MainAdapter(TaskList.taskList)

        //set how RecyclerView items will be displayed (here - on top of each other)
        /**This setting can also be alternatively configured in layout xml file for RecyclerView
        item using "app:layoutManager"="androidx.recyclerview.widget.LinearLayoutManager"
        and was configured there in this particular project, so here in code it's commentedOut*/
//      binding?.taskRecyclerView?.layoutManager=LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)

        //Connect activity_main layout object "@+id/taskRecyclerView" with the prepared adapter
        binding?.taskRv?.adapter = adapter
    }

    //Cleanup when finished the activity to avoid memory leak
    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }
}