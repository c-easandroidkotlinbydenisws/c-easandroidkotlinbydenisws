package com.example.mycalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private var tvInput: TextView? = null
    var lastNumeric: Boolean = false
    var lastDot: Boolean = false
    var firstDot: Boolean = false
    var mainOperatorEntered = false
    var mainOperatorSymbol: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvInput = findViewById(R.id.tvInput)
    }

    fun onDigit(view: View){
//        Toast.makeText(this, "Button clicked", Toast.LENGTH_LONG).show()
        /* in the next statement the view object is converted to a button.
        then using the text property is possible to retrieve
        the button label(its text).
         */
        tvInput?.append((view as Button).text)
//        lastDot = false
        lastNumeric = true
    }

    fun onClear(view: View){
        tvInput?.text = ""
        lastDot = false
        lastNumeric = false
        firstDot = false
        mainOperatorEntered = false
    }

    fun onDecimalPoint(view: View){
        if((lastNumeric && !lastDot) || !firstDot){
            tvInput?.append((view as Button).text)
            lastDot = true
            lastNumeric = false
            firstDot = true
        }
    }

    fun onOperator(view: View){
        val value = (view as Button).text
        if (value == "-" && //minus is the first char (unary).
            (tvInput?.text.toString() == null ||
            tvInput?.length() == 0)){
            rendOperator(view)
            return
        }

        if (value == "-" && //minus as the main operator
            lastNumeric){
            rendOperator(view)
            mainOperatorEntered = true
            return
        }

        if (value == "-" && //minus after main operator.
            mainOperatorEntered){
            rendOperator(view)
            mainOperatorEntered = false
            return
        }

        if ((value == "+" || //the rest of operators.
            value == "*" ||
            value == "/") &&
            tvInput?.length() != 0 &&
            !mainOperatorEntered){
            rendOperator(view)
            mainOperatorEntered = true
            return
        }
    }

    private fun rendOperator(view: View){
        tvInput?.append((view as Button).text)
        lastDot = false
        lastNumeric = false
        firstDot = false
    }

    fun onEqual(view: View){
        if(lastNumeric){
            val tvValue = tvInput?.text.toString()

            try{
                val splitValue = tvValue.split("-")
//                val splitValue = tvValue.split("-").toTypedArray()
//                println("tvValue: $tvValue")
//                println("splitValue: ${splitValue.contentToString()} Size: ${splitValue.size}")
//                val itr = tvValue.iterator()
//                while(itr.hasNext()){
//                    println(itr.next())
//                }
                var indexToSplit: Int = 0
                for ((index, value) in tvValue.withIndex()) {
                    if(index != 0){
//                        println("the element at $index is $value")
                        if(value.equals('+') ||
                           value.equals('-') ||
                           value.equals('*') ||
                           value.equals('/')){
                            indexToSplit = index
                            mainOperatorSymbol = value.toString()
                            break
                        }
                    }
                }
                val one: String = tvValue.subSequence(0, indexToSplit).toString()
                val two = tvValue.drop(one.length + 1)
//                println("expression total size: ${tvValue.length}")
//                println("the first sub is: ${one.toString()} Size: ${one.length}")
//                println("the second sub is: ${two.toString()} Size: ${two.length}")
                when{
                    mainOperatorSymbol == "/" -> tvInput?.text =
                        removeZeroAfterDot(String.format("%.2f", one.toDouble() / two.toDouble()))
                    mainOperatorSymbol == "*" -> tvInput?.text =
                        removeZeroAfterDot(String.format("%.2f", one.toDouble() * two.toDouble()))
                    mainOperatorSymbol == "-" -> tvInput?.text =
                        removeZeroAfterDot(String.format("%.2f", one.toDouble() - two.toDouble()))
                    else -> tvInput?.text =
                        removeZeroAfterDot(String.format("%.2f", one.toDouble() + two.toDouble()))
                }
                mainOperatorEntered = false
            }catch(e: ArithmeticException){
                e.printStackTrace()
            }
        }
    }

    private fun removeZeroAfterDot(result: String): String{
        var value = result
        if(result.contains(".0")){
            value = result.substring(0, result.length - 2)
        }
        return value
    }
//    private fun isOperatorAdded(value: String): Boolean {
//        return if (value.startsWith("-")) {
//            false
//        } else {
//            value.contains("/") ||
//            value.contains("*") ||
//            value.contains("+") ||
//            value.contains("-")
//            /* next sentences were coded to show
//             the contains() in action. Move them form here to test.
//              */
////        var sentence = "orlando is learning kotlin"
////        if(sentence.contains("kotlin")){
////            tvInput?.append("orlando")
////        }
//        }
//    }
}