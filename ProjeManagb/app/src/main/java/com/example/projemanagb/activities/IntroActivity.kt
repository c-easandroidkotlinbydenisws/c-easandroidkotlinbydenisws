package com.example.projemanagb.activities

import android.content.Intent
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.example.projemanagb.databinding.ActivityIntroBinding

//  Add the Intro Activity.
class IntroActivity : BaseActivity() {
    private var binding: ActivityIntroBinding? = null

    /**
     * This function is auto created by Android when the Activity Class is created.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        //This call the parent constructor
        super.onCreate(savedInstanceState)
        // This is used to align the xml view to this class
        binding = ActivityIntroBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        // This is used to hide the status bar and make the splash screen as a full screen activity.
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)

        val typeface: Typeface =
            Typeface.createFromAsset(assets, "carbon_bl.ttf")
        binding?.tvAppNameIntro?.typeface = typeface

        // Add a click event for Sign Up btn and launch the Sign Up Screen.
        binding?.btnSignUpIntro?.setOnClickListener(){
            startActivity(Intent(this@IntroActivity, SignUpActivity::class.java))
        }

        // Add a click event for Sign In btn and launch the Sign In Screen.
        binding?.btnSignInIntro?.setOnClickListener {
            // Launch the sign in screen.
            startActivity(Intent(this@IntroActivity, SignInActivity::class.java))
        }

    }
}