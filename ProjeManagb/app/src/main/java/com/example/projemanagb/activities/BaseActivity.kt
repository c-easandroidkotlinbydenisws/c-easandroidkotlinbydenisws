// Here we have created a BaseActivity Class in which we have added the progress dialog and SnackBar.
//  Now all the activity will extend the BaseActivity instead of AppCompatActivity.
package com.example.projemanagb.activities

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.core.content.ContextCompat
import com.example.projemanagb.R
import com.example.projemanagb.databinding.ActivityBaseBinding
import com.example.projemanagb.databinding.DialogProgressBinding
import com.example.projemanagb.utils.toast
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth

open class BaseActivity : AppCompatActivity() {
    private var binding: ActivityBaseBinding? = null
    private var binding2: DialogProgressBinding? = null
    private var doubleBackToExitPressedOnce = false //for tracking the back button functionality.
                                                    //if the back button is pressed twice, the app is closed.
    /**
     * This is a progress dialog instance which we will initialize later on.
     */
    private lateinit var mProgressDialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBaseBinding.inflate(layoutInflater)
        binding2 = DialogProgressBinding.inflate(layoutInflater)
    }

    /**
     * This function is used to show the progress dialog with the title and message to the user.
     */
    fun showProgressDialog(text: String) {
        mProgressDialog = Dialog(this)
        /*Set the screen content from a layout resource.
        The resource will be inflated, adding all top-level views to the screen.*/
        mProgressDialog.setContentView((binding2?.root!!))
        binding2?.tvProgressText?.text = text
        mProgressDialog.setContentView(R.layout.dialog_progress)
        //Start the dialog and display it on screen.
        mProgressDialog.show()
    }

    /**
     * This function is used to dismiss the progress dialog if it is visible to user.
     */
    fun hideProgressDialog() {
        mProgressDialog.dismiss()
    }

    fun getCurrentUserID(): String {
        return FirebaseAuth.getInstance().currentUser!!.uid
    }

    fun doubleBackToExit() {
        if (doubleBackToExitPressedOnce) {
//            super.onBackPressed()
            onBackPressedDispatcher.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        toast(R.string.please_click_back_again_to_exit)

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    fun showErrorSnackBar(message: String) {
        val snackBar =
            Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG)
        val snackBarView = snackBar.view
        snackBarView.setBackgroundColor(
            ContextCompat.getColor(
                this@BaseActivity,
                R.color.snackbar_error_color
            )
        )
        snackBar.show()
    }
}