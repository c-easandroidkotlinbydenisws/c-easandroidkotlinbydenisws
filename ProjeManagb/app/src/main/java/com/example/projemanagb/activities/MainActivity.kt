package com.example.projemanagb.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.projemanagb.databinding.ActivityMainBinding
class MainActivity : AppCompatActivity() {
    private var binding: ActivityMainBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)
//        setContentView(R.layout.activity_main)

//        toast(R.string.any_string)
    }

    override fun onDestroy() {
        super.onDestroy()
        //TO avoid memory leak we disallow the binding once the activity is destroyed
        binding = null
    }
}