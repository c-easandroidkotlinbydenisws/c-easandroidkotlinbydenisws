package com.example.projemanagb.activities

import android.content.Intent
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.example.projemanagb.databinding.ActivitySplashBinding
import android.os.Handler
import com.example.projemanagb.firebase.FirestoreClass

// Add new activity named as splash activity.
class SplashActivity : AppCompatActivity() {
    private var binding: ActivitySplashBinding? = null

    /**
     * This function is auto created by Android when the Activity Class is created.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        //This call the parent constructor
        super.onCreate(savedInstanceState)
        // This is used to align the xml view to this class
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        // Add the full screen flags here.
        // This is used to hide the status bar and make the splash screen as a full screen activity.
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)

        // Add the file in the custom font file to the assets folder. And add the below line of code to apply it to the title TextView.
        // Steps for adding the assets folder are :
        // Right click on the "app" package and GO TO ==> New ==> Folder ==> Assets Folder ==> Finish.
        // This is used to get the file from the assets folder and set it to the title textView.
        val typeface: Typeface =
            Typeface.createFromAsset(assets, "carbon_bl.ttf")
        binding?.tvAppName?.typeface = typeface

        // Here we will launch the Intro Screen after the splash screen using the handler. As using handler the splash screen will disappear after what we give to the handler.
        // Adding the handler to after the a task after some delay.
//        Handler().postDelayed({
//            // Start the Intro Activity
//            startActivity(Intent(this@SplashActivity, IntroActivity::class.java))
//            finish() // Call this when your activity is done and should be closed.
//        }, 2500) // Here we pass the delay time in milliSeconds after which the splash activity will disappear.
        Handler().postDelayed({
            // Check if the current user id is not blank then send the user to MainActivity as he have logged in
            //  before or else send him to Intro Screen as earlier.
            // Here if the user is signed in once and not signed out again from the app. So next time while coming into the app
            // we will redirect him to MainScreen or else to the Intro Screen as it was before.
            // Get the current user id
            val currentUserID = FirestoreClass().getCurrentUserID()
            if (currentUserID.isNotEmpty()) {
                // Start the Main Activity
                startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            } else {
                // Start the Intro Activity
                startActivity(Intent(this@SplashActivity, IntroActivity::class.java))
            }
            finish() // Call this when your activity is done and should be closed.
        }, 2500) // Here we pass the delay time in milliSeconds after which the splash activity will disappear.
    }
}