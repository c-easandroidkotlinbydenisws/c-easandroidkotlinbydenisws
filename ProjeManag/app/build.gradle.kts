plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    // Add a plugin here
    id("com.google.gms.google-services")
}

android {
    namespace = "com.example.projemanag"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.example.projemanag"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = org.gradle.api.JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        compose = true
        viewBinding = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {
    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.7.0")
    implementation("androidx.activity:activity-compose:1.8.2")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.11.0")
    // Firebase Authentication dependency.
    implementation("com.google.firebase:firebase-auth-ktx:22.3.1")
    // Firebase Cloud Firestore Dependency
    implementation("com.google.firebase:firebase-firestore:24.11.0")
    // Firebase Storage Dependency
    implementation("com.google.firebase:firebase-storage:20.3.0")
    // circular image view dependency
    implementation("de.hdodenhof:circleimageview:3.1.0")
    // Add a Image Loading library ( a third party lib).
    implementation("com.github.bumptech.glide:glide:4.12.0")
    annotationProcessor("com.github.bumptech.glide:compiler:4.11.0")
    // Add a firebase messaging dependency.
    // For more details visit link: https://firebase.google.com/docs/cloud-messaging/android/client
    // Firebase Messaging Dependency
    implementation("com.google.firebase:firebase-messaging:23.4.1")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
}