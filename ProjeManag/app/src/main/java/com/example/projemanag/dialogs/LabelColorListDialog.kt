package com.example.projemanag.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.projemanag.adapters.LabelColorListItemsAdapter
import com.example.projemanag.databinding.DialogListBinding

// Create an dialogs package and a class for showing the label color list dialog.
abstract class LabelColorListDialog(
    context: Context,
    private var list: ArrayList<String>,
    private val title: String = "",
    private val mSelectedColor: String = "") : Dialog(context) {
        //this class is extending the Dialog class.
        private var binding: DialogListBinding? = null
        private var adapter: LabelColorListItemsAdapter? = null

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState ?: Bundle())
            binding = DialogListBinding.inflate(layoutInflater)
            binding?.root?.let { setContentView(it) }//it applies to binding?.root?
//            val view = LayoutInflater.from(context).inflate(R.layout.dialog_list, null)
//            setContentView(view)
            setCanceledOnTouchOutside(true)
            setCancelable(true)
//            setUpRecyclerView(view)
            binding?.root?.let { setUpRecyclerView(it) }
        }

        private fun setUpRecyclerView(view: View) {
                binding?.tvTitle?.text = title
                binding?.rvList?.layoutManager = LinearLayoutManager(context)
                adapter = LabelColorListItemsAdapter(context, list, mSelectedColor)
                binding?.rvList?.adapter = adapter

                adapter!!.onItemClickListener = object : LabelColorListItemsAdapter.OnItemClickListener {
                    override fun onClick(position: Int, color: String) {
                        dismiss()
                        onItemSelected(color)
                    }
        }
                                                                                                                    }
        protected abstract fun onItemSelected(color: String)
}