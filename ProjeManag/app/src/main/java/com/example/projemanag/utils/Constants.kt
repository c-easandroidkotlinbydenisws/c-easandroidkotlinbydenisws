package com.example.projemanag.utils

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import android.webkit.MimeTypeMap
import com.example.projemanag.activities.MyProfileActivity

object Constants {

    // Firebase Constants
    // This  is used for the collection name for USERS.
    const val USERS: String = "users"
    // This  is used for the collection name for BOARDS.
    const val BOARDS: String = "boards"
    // Firebase database field names
    const val IMAGE: String = "image"
    const val NAME: String = "name"
    const val MOBILE: String = "mobile"
    // Add a field name as assignedTo which we are gonna use later on.
    const val ASSIGNED_TO: String = "assignedTo"
    // Add constant for DocumentId
    const val DOCUMENT_ID: String = "documentId"
    // Add a new field for TaskList.
    const val TASK_LIST: String = "taskList"
    // Add field name as a constant which we will be using for getting the list of user details from the database.
    const val ID: String = "id"
    const val EMAIL: String = "email"
    // Add constant for passing the board details through intent.
    const val BOARD_DETAIL: String = "board_detail"
    // Add all the required constants for passing the details to CardDetailsActivity through intent.
    const val TASK_LIST_ITEM_POSITION: String = "task_list_item_position"
    const val CARD_LIST_ITEM_POSITION: String = "card_list_item_position"
    // Add the constant here.
    const val BOARD_MEMBERS_LIST: String = "board_members_list"
    // Add the constants here.
    const val SELECT: String = "Select"
    const val UN_SELECT: String = "UnSelect"
    // Move all constants and function here and make them public and change accordingly.
    //A unique code for asking the Read Storage Permission using this we will be check and identify in the method onRequestPermissionsResult
    const val READ_STORAGE_PERMISSION_CODE = 1
    // A unique code of image selection from Phone Storage.
    const val PICK_IMAGE_REQUEST_CODE = 2
    // Add a SharedPreferences name and key names.
    const val PROJEMANAG_PREFERENCES: String = "ProjemanagPrefs"
    const val FCM_TOKEN:String = "fcmToken"
    const val FCM_TOKEN_UPDATED:String = "fcmTokenUpdated"
    // Add the base url  and key params for sending firebase notification.
    const val FCM_BASE_URL:String = "https://fcm.googleapis.com/fcm/send"//if you want to send something.
    const val FCM_AUTHORIZATION:String = "authorization"
    const val FCM_KEY: String = "key"
    //next one: your own server key
    const val FCM_SERVER_KEY:String = "AAAA1M0gt48:APA91bGwveFki-vvdkph1E1SxUjaQW6Edw79Nwi7DNv9SKrX67pxXvBY4WKklO6kvSujubnJQkl8PMZOzePBp9Ye34K4VnsMhVM3vdYKb7UohZMlmmTk-d6tfMcsefEkml56PfjjlGxf"
    const val FCM_KEY_TITLE:String = "title"
    const val FCM_KEY_MESSAGE:String = "message"
    const val FCM_KEY_DATA:String = "data"
    const val FCM_KEY_TO:String = "to"

    /**
     * A function for user profile image selection from phone storage.
     */
    fun showImageChooser(activity: Activity) {
        // An intent for launching the image selection of phone storage.
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        // Launches the image selection of phone storage using the constant code.
        activity.startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST_CODE) //startActivityForResult() is java method of Activity java class.
    }

    /**
     * A function to get the extension of selected image.
     */
    fun getFileExtension(activity: Activity, uri: Uri?): String? {
        /*
         * MimeTypeMap: Two-way map that maps MIME-types to file extensions and vice versa.
         *
         * getSingleton(): Get the singleton instance of MimeTypeMap.
         *
         * getExtensionFromMimeType: Return the registered extension for the given MIME type.
         *
         * contentResolver.getType: Return the MIME type of the given content URL.
         */
        return MimeTypeMap.getSingleton()
            .getExtensionFromMimeType(activity.contentResolver.getType(uri!!))
    }
}
