package com.example.projemanag.activities

import android.os.Bundle
import android.text.TextUtils
import android.view.WindowManager
import com.example.projemanag.R
import com.example.projemanag.databinding.ActivitySignUpBinding
import com.example.projemanag.firebase.FirestoreClass
import com.example.projemanag.models.User
import com.example.projemanag.utils.toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

// Add the sign up activity.
class SignUpActivity : BaseActivity() {
    private var binding: ActivitySignUpBinding? = null

    /**
     * This function is auto created by Android when the Activity Class is created.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        //This call the parent constructor
        super.onCreate(savedInstanceState)
        // This is used to align the xml view to this class
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        // This is used to hide the status bar and make the splash screen as a full screen activity.
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)

        // Call the setup actionBar function.
        setupActionBar()

        // Add a click event to the Sign-Up button and call the registerUser function.
        // Click event for sign-up button.
        binding?.btnSignUp?.setOnClickListener{
            registerUser()
        }
    }

    /**
     * A function for actionBar Setup.
     */
    private fun setupActionBar() {
        setSupportActionBar(binding?.toolbarSignUpActivity)

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeAsUpIndicator(R.drawable.ic_black_color_back_24dp)
        }

        binding?.toolbarSignUpActivity?.setNavigationOnClickListener {
//            onBackPressed()
            onBackPressedDispatcher.onBackPressed()
        }
    }

    /**
     * A function to register a user to our app using the Firebase.
     * For more details visit: https://firebase.google.com/docs/auth/android/custom-auth
     */
    private fun registerUser(){
        val name: String = binding?.etName?.text.toString().trim { it <= ' ' }
        val email: String = binding?.etEmailSignUp?.text.toString().trim { it <= ' ' }
        val password: String = binding?.etPasswordSignUp?.text.toString().trim { it <= ' ' }

        if (validateForm(name, email, password)) {
//            toast(R.string.register_a_new_user) //valid before firebase.this
            // Show the progress dialog.
            showProgressDialog(resources.getString(R.string.please_wait))//from BaseActivity.kt
            //this creates a new user in authentication from the Firebase platform.
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(
                    OnCompleteListener<AuthResult> { task ->
                        // Hide the progress dialog
//                        hideProgressDialog() //commented after userRegisteredSuccess() was implemented.
                        // If the registration is successfully done
                        if (task.isSuccessful) {
                            // Firebase registered user
                            val firebaseUser: FirebaseUser = task.result!!.user!!
                            // Registered Email
                            val registeredEmail = firebaseUser.email!!
//                            toast(getString(R.string.successfully_registered_with_email_id,
//                                    name,
//                                    registeredEmail))//commented after userRegisteredSuccess() was implemented.
                            //this creates a User object with the basic info (ui, name and email)
                            val user = User(
                                firebaseUser.uid, name, registeredEmail
                            )
                            /**
                             * Here the new user registered is automatically signed-in so we just sign-out the user from firebase
                             * and send him to Intro Screen for Sign-In
                             */
//                            FirebaseAuth.getInstance().signOut()//commented after userRegisteredSuccess() was implemented.
//                            // Finish the Sign-Up Screen
//                            finish()//commented after userRegisteredSuccess() was implemented.
                            // call the registerUser function of FirestoreClass to make an entry in the database.
                            FirestoreClass().registerUser(this@SignUpActivity, user)
                        } else {
                            toast(task.exception!!.message.toString())
                        }
                    })
        }
    }

    /**
     * A function to validate the entries of a new user.
     */
    private fun validateForm(name: String, email: String, password: String): Boolean {
        return when {
            TextUtils.isEmpty(name) -> {
                showErrorSnackBar("Please enter name.")
                false
            }
            TextUtils.isEmpty(email) -> {
                showErrorSnackBar("Please enter email.")
                false
            }
            TextUtils.isEmpty(password) -> {
                showErrorSnackBar("Please enter password.")
                false
            }
            else -> {
                true
            }
        }
    }

    /**
     * A function to be called the user is registered successfully and entry is made in the firestore database.
     */
    fun userRegisteredSuccess() {
        toast(R.string.successfully_registered)
        // Hide the progress dialog
        hideProgressDialog()
        /**
         * Here the new user registered is automatically signed-in so we just sign-out the user from firebase
         * and send him to Intro Screen for Sign-In
         */
        FirebaseAuth.getInstance().signOut()
        // Finish the Sign-Up Screen
        finish()
    }
}