package com.example.projemanag.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.projemanag.R
import com.example.projemanag.models.SelectedMembers
import com.example.projemanag.databinding.ItemCardSelectedMemberBinding

open class CardMemberListItemsAdapter(
    private val context: Context,
    private var list: ArrayList<SelectedMembers>,
    private val assignMembers: Boolean): RecyclerView.Adapter<CardMemberListItemsAdapter.MyViewHolder>() {
        private var onClickListener: OnClickListener? = null

        /**
         * Inflates the item views which is designed in xml layout file
         *
         * create a new
         * {@link ViewHolder} and initializes some private fields to be used by RecyclerView.
         */
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            return MyViewHolder(ItemCardSelectedMemberBinding.inflate(LayoutInflater.from(parent.context)))
        }

        /**
         * Binds each item in the ArrayList to a view
         *
         * Called when RecyclerView needs a new {@link ViewHolder} of the given type to represent
         * an item.
         *
         * This new ViewHolder should be constructed with a new View that can represent the items
         * of the given type. You can either create a new View manually or inflate it from an XML
         * layout file.
         */
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val model = list[position]

            if (holder is MyViewHolder) {
                if (position == list.size - 1 && assignMembers) {
                    holder.binding?.ivAddMember?.visibility = View.VISIBLE
                    holder.binding?.ivSelectedMemberImage?.visibility = View.GONE
                } else {
                    holder.binding?.ivAddMember?.visibility = View.GONE
                    holder.binding?.ivSelectedMemberImage?.visibility = View.VISIBLE

                    holder.binding?.ivSelectedMemberImage?.let {
                        Glide
                            .with(context)
                            .load(model.image)
                            .centerCrop()
                            .placeholder(R.drawable.ic_user_place_holder)
                            .into(it)
                    }
                }

                holder.itemView.setOnClickListener {
                    if (onClickListener != null) {
                        onClickListener!!.onClick()
                    }
                }
            }
        }

        override fun getItemCount(): Int {
            return list.size
        }

                                                        /**
         * A function for OnClickListener where the Interface is the expected parameter..
         */
        fun setOnClickListener(onClickListener: OnClickListener) {
            this.onClickListener = onClickListener
        }

        /**
         * An interface for onclick items.
         */
        interface OnClickListener {
            fun onClick()
        }

        /**
         * A ViewHolder describes an item view and metadata about its place within the RecyclerView.
         */
        class MyViewHolder(val binding: ItemCardSelectedMemberBinding): RecyclerView.ViewHolder(binding.root)
}