package com.example.projemanag.adapters
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.projemanag.databinding.ItemLabelColorBinding

// Create an adapter class for selection of card label color using the "item_label_color".
class LabelColorListItemsAdapter(
    private val context: Context,
    private var list: ArrayList<String>,
    private val mSelectedColor: String): RecyclerView.Adapter<LabelColorListItemsAdapter.MyViewHolder>(){
        var onItemClickListener: OnItemClickListener? = null

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val binding = ItemLabelColorBinding.inflate(LayoutInflater.from(parent.context))
            return MyViewHolder((binding))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = list[position]

        if (holder is MyViewHolder) {
            holder.binding?.viewMain?.setBackgroundColor(Color.parseColor(item))

            if (item == mSelectedColor) {
                holder.binding?.ivSelectedColor?.visibility = View.VISIBLE
            } else {
                holder.binding?.ivSelectedColor?.visibility = View.GONE
            }

            holder.binding.root.setOnClickListener {
                if (onItemClickListener != null) {
                    onItemClickListener!!.onClick(position, item)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(val binding: ItemLabelColorBinding): RecyclerView.ViewHolder(binding.root) {
    }

    interface OnItemClickListener {
        fun onClick(position: Int, color: String)
    }
}