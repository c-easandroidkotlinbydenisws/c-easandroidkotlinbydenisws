package com.example.projemanag.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.projemanag.adapters.MemberListItemsAdapter
import com.example.projemanag.models.User
import com.example.projemanag.databinding.DialogListBinding

// Create a members list dialog class to show the list of members in a dialog.
abstract class MembersListDialog(
    context: Context,
    private var list: ArrayList<User>,
    private val title: String = "") : Dialog(context) {
        private var binding: DialogListBinding? = null
        private var adapter: MemberListItemsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())
        binding = DialogListBinding.inflate(layoutInflater)
        binding?.root?.let { setContentView(it) }//it applies to binding?.root?

//        val view = LayoutInflater.from(context).inflate(R.layout.dialog_list, null)
//        setContentView(view)
        setCanceledOnTouchOutside(true)
        setCancelable(true)
//        setUpRecyclerView(view)
        binding?.root?.let { setUpRecyclerView(it) }
    }

    private fun setUpRecyclerView(view: View) {
        binding?.tvTitle?.text = title

        if (list.size > 0) {
            binding?.rvList?.layoutManager = LinearLayoutManager(context)
            adapter = MemberListItemsAdapter(context, list)
            binding?.rvList?.adapter = adapter

            adapter!!.setOnClickListener(object :
                MemberListItemsAdapter.OnClickListener {
                override fun onClick(position: Int, user: User, action:String) {
                    dismiss()
                    onItemSelected(user, action)
                }
            })
        }
    }

    protected abstract fun onItemSelected(user: User, action:String)
}