package com.example.projemanag.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.projemanag.R
import com.example.projemanag.adapters.BoardItemsAdapter
import com.example.projemanag.databinding.ActivityMainBinding
import com.example.projemanag.databinding.AppBarMainBinding
import com.example.projemanag.databinding.NavHeaderMainBinding
import com.example.projemanag.firebase.FirestoreClass
import com.example.projemanag.models.Board
import com.example.projemanag.models.User
import com.example.projemanag.utils.Constants
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging

// Implement the NavigationView.OnNavigationItemSelectedListener and add the implement members of it.
class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {
    private var binding: ActivityMainBinding? = null
    private var binding2: AppBarMainBinding? = null
    // Create a global variable for user name
    private lateinit var mUserName: String
    // A global variable for SharedPreferences
    private lateinit var mSharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        binding2 = binding?.appBarMain

        // Call the setup action bar function here.
        setupActionBar()

        // Assign the NavigationView.OnNavigationItemSelectedListener to navigation view.
        binding?.navView?.setNavigationItemSelectedListener(this)

        // Initialize the mSharedPreferences variable.
        mSharedPreferences =
            this.getSharedPreferences(Constants.PROJEMANAG_PREFERENCES, Context.MODE_PRIVATE)
        // Get the FCM token and update it in the database.
        // Variable is used get the value either token is updated in the database or not.
        val tokenUpdated = mSharedPreferences.getBoolean(Constants.FCM_TOKEN_UPDATED, false)
        // Here if the token is already updated than we don't need to update it every time.
        if (tokenUpdated) {
            // Get the current logged in user details.
            // Show the progress dialog.
            showProgressDialog(resources.getString(R.string.please_wait))
            FirestoreClass().loadUserData(this@MainActivity, true)
        } else {
            FirebaseMessaging.getInstance().token
                .addOnSuccessListener(this@MainActivity) {
                    updateFCMToken(it)
                }
        }

        // Show the progress dialog.
//        showProgressDialog(resources.getString(R.string.please_wait))
        // Get the current logged in user details.
        // Here pass the parameter value as TRUE to get the board list, rest all are FALSE.
//        FirestoreClass().loadUserData(this@MainActivity, true)
//        hideProgressDialog()
        // Launch the Create Board screen on a fab button click.
        binding2?.fabCreateBoard?.setOnClickListener {
//            startActivity(Intent(this@MainActivity, CreateBoardActivity::class.java))
            // Pass the user name through intent to CreateBoardScreen.
            val intent = Intent(this@MainActivity, CreateBoardActivity::class.java)
            intent.putExtra(Constants.NAME, mUserName)
//            startActivity(intent)
            // Here now pass the unique code for StartActivityForResult.
            startActivityForResult(intent, CREATE_BOARD_REQUEST_CODE)
        }
    }

    // Add a onBackPressed function and check if the navigation drawer is open or closed.
    @SuppressLint("MissingSuperCall")
    override fun onBackPressed() {
        if (binding?.drawerLayout?.isDrawerOpen(GravityCompat.START) == true) {
            binding?.drawerLayout?.closeDrawer(GravityCompat.START)
        } else {
            // A double back press function is added in Base Activity.
            doubleBackToExit()//thgis comes from the BasicActivity.kt.
        }
    }

    // Implement members of NavigationView.OnNavigationItemSelectedListener.
    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        // Add the click events of navigation menu items.
        when (menuItem.itemId) {
            R.id.nav_my_profile -> {
//                toast(R.string.my_profile)
                // Launch the MyProfileActivity Screen.
//                startActivity(Intent(this@MainActivity, MyProfileActivity::class.java))
                startActivityForResult(Intent(this@MainActivity, MyProfileActivity::class.java), MY_PROFILE_REQUEST_CODE)
            }

            R.id.nav_sign_out -> {
                // Here sign outs the user from firebase in this device.
                FirebaseAuth.getInstance().signOut()
                // Clear the shared preferences when the user signOut.
                mSharedPreferences.edit().clear().apply()

                // Send the user to the intro screen of the application.
                val intent = Intent(this, IntroActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            }
        }
        binding?.drawerLayout?.closeDrawer(GravityCompat.START)
        return true
    }

    // Add the onActivityResult function and check the result of the activity for which we expect the result.
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK
            && requestCode == MY_PROFILE_REQUEST_CODE
        ) {// Get the user updated details.
            FirestoreClass().loadUserData(this@MainActivity)
            // Here if the result is OK get the updated boards list.
        } else if (resultCode == Activity.RESULT_OK
            && requestCode == CREATE_BOARD_REQUEST_CODE
        ) {// Get the latest boards list.
            FirestoreClass().getBoardsList(this@MainActivity)
        } else {
            Log.e("Cancelled", "Cancelled")
        }
    }

    /**
     * A function to setup action bar
     */
    private fun setupActionBar() {
        setSupportActionBar(binding2?.toolbarMainActivity)
        binding2?.toolbarMainActivity?.setNavigationIcon(R.drawable.ic_action_navigation_menu)

        // Add click event for navigation in the action bar and call the toggleDrawer function.
        binding2?.toolbarMainActivity?.setNavigationOnClickListener {
            toggleDrawer()
        }
    }

    /**
     * A function for opening and closing the Navigation Drawer.
     */
    private fun toggleDrawer() {
        if (binding?.drawerLayout?.isDrawerOpen(GravityCompat.START) == true) {
            binding?.drawerLayout?.closeDrawer(GravityCompat.START)
        } else {
            binding?.drawerLayout?.openDrawer(GravityCompat.START)
        }
    }

    // Create a function to update the user details in the navigation view.
    /**
     * A function to get the current user details from firebase.
     */
    // Add a parameter to check whether to read the boards list or not.
    fun updateNavigationUserDetails(user: User, isToReadBoardsList: Boolean) {
        hideProgressDialog()
        // Initialize the UserName variable.
        mUserName = user.name
        // The instance of the header view of the navigation view.
        // navView is NavigationView.
        val headerView = binding?.navView?.getHeaderView(0)
        // nav_header_main.xml is headerLayout
        val navViewHeaderBinding: NavHeaderMainBinding = NavHeaderMainBinding.bind(headerView!!)
        // The instance of the user image of the navigation view.
        val navUserImage = navViewHeaderBinding.ivUserImage
        // Initialize the UserName variable.
        mUserName = user.name
        // Load the user image in the ImageView.
        Glide
            .with(this@MainActivity)
            .load(user.image)
//                .load("https://file-examples.com/storage/fe34a88a9a65cf545955ccb/2017/10/file_example_PNG_500kB.png")//works
//                .load("https://upload.wikimedia.org/wikipedia/commons/3/36/Red_jungle_fowl_white_background.png") //works
            .centerCrop() // Scale type of the image.
            .placeholder(R.drawable.ic_user_place_holder) // A default place holder
            .into(navUserImage)// the view in which the image will be loaded.
        // The instance of the user name TextView of the navigation view.
//        val navUsername = navViewHeaderBinding.tvUsername
//        navUsername.text = user.name
        // Set the user name
        navViewHeaderBinding.tvUsername.text = user.name

        // Here if the isToReadBoardList is TRUE then get the list of boards.)
        if (isToReadBoardsList) {
            // Show the progress dialog.
            showProgressDialog(resources.getString(R.string.please_wait))
            FirestoreClass().getBoardsList(this@MainActivity)
        }
    }

    /**
     * A function to update the user's FCM token into the database.
     */
    private fun updateFCMToken(token: String) {
        val userHashMap = HashMap<String, Any>()
        userHashMap[Constants.FCM_TOKEN] = token
        // Update the data in the database.
        // Show the progress dialog.
        showProgressDialog(resources.getString(R.string.please_wait))
        FirestoreClass().updateUserProfileData(this@MainActivity, userHashMap)
    }

    /**
     * A function to populate the result of BOARDS list in the UI i.e in the recyclerView.
     */
    fun populateBoardsListToUI(boardsList: ArrayList<Board>) {
        hideProgressDialog()

        if (boardsList.size > 0) {
            binding2?.contentMain?.rvBoardsList?.visibility = View.VISIBLE
            binding2?.contentMain?.tvNoBoardsAvailable?.visibility = View.GONE

            binding2?.contentMain?.rvBoardsList?.layoutManager = LinearLayoutManager(this@MainActivity)
            binding2?.contentMain?.rvBoardsList?.setHasFixedSize(true)

            // Create an instance of BoardItemsAdapter and pass the boardList to it.
            val adapter = BoardItemsAdapter(this@MainActivity, boardsList)
            binding2?.contentMain?.rvBoardsList?.adapter = adapter // Attach the adapter to the recyclerView.

            // Add click event for boards item and launch the TaskListActivity
            adapter.setOnClickListener(object: BoardItemsAdapter.OnClickListener {
                override fun onClick(position: Int, model: Board) {
                    // Pass the documentId of a board through intent.
                    val intent = Intent(this@MainActivity, TaskListActivity::class.java)
//                    startActivity(Intent(this@MainActivity, TaskListActivity::class.java))
                    intent.putExtra(Constants.DOCUMENT_ID, model.documentId)
                    startActivity(intent)
                }
            })
        } else {
            binding2?.contentMain?.rvBoardsList?.visibility = View.GONE
            binding2?.contentMain?.tvNoBoardsAvailable?.visibility = View.VISIBLE
        }
    }

    /**
     * A function to notify the token is updated successfully in the database.
     */
    fun tokenUpdateSuccess() {//we got the token when the user is registered or login in.
        hideProgressDialog()
        // Here we have added a another value in shared preference that the token is updated in the database successfully.
        // So we don't need to update it every time.
        val editor: SharedPreferences.Editor = mSharedPreferences.edit()
        editor.putBoolean(Constants.FCM_TOKEN_UPDATED, true)
        editor.apply()
        // Get the current logged in user details.
        // Show the progress dialog.
        showProgressDialog(resources.getString(R.string.please_wait))
        FirestoreClass().loadUserData(this@MainActivity, true)
    }

    override fun onDestroy() {
        super.onDestroy()
        //TO avoid memory leak we disallow the binding once the activity is destroyed
        binding = null
    }

    /**
     * A companion object to declare the constants.
     */
    companion object {
        //A unique code for starting the activity for result
        const val MY_PROFILE_REQUEST_CODE: Int = 11
        //Add a unique code for starting the create board activity for result
        const val CREATE_BOARD_REQUEST_CODE: Int = 12
    }
}