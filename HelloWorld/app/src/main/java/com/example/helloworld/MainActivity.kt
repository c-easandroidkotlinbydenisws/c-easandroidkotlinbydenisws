package com.example.helloworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.helloworld.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(getLayoutInflater())
        //setContentView(R.layout.activity_main)
        setContentView(binding.root)

        // get reference to textview
        //val btnClickMe = findViewById(R.id.button) as Button
        //val myTextView = findViewById(R.id.textView) as TextView
        var timesClicked = 1
        // set on-click listener
        binding.button.setOnClickListener {
            // your code to run when the user clicks on the button
            binding.textView.text = timesClicked.toString()
            timesClicked += 1
            Toast.makeText(this@MainActivity, "Hi Orlando!", Toast.LENGTH_SHORT).show()
        }
    }
}