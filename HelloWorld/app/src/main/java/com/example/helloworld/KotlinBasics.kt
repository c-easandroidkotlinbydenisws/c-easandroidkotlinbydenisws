package com.example.helloworld

fun main() {
//    print("Hello World!")
//    var myName = "Orlando"
//    myName = "lina"
//    print("Hello " + myName + "!")
//
//    val yourName = "Orlando"
//    yourName = "lina"
//    print("Hello " + myName + "!")
    //todo: add new funcionality.
    //string.
//    val myName = "Orlando"
//    print("Hello " + myName + "!")
//    //int 32 bit
//    var myAge = 31
//    //byte (8 bit).
//    val myByte: Byte = 13
//    //short (16 bit).
//    val myShort: Short = 125
//    //int (32 bit).
//    val myInt: Int= 123123123  //this is the default.
//    //long(64 bit).
//    val myLong: Long= 39_812_309_487_120_3
//    print("long integer: " + myLong)
//
//    //float (32 bit).
//    val myFloat: Float = 13.37f //if you miss the “F”, it’s considered double.
//    //double (64 bit).
//    val myDouble: Double = 3.14159265358979323846
//
//    //boolean.
//    var isSunny: Boolean = true
//
//    //characters.
//    val letterChar = 'A'
//    val digitChar = '$'
//
//    //more about strings.
//    val myStr = "Hello World"
//    var firstCharInStr = myStr[0]
//    var lastCharInStr = myStr[myStr.length - 1]
//    print("Hello " + firstCharInStr + "!") //prints Hello H!
//    print("Hello " + lastCharInStr + "!") //prints Hello d!
//
//    //exercise:
//    var myCreditCard: String = "Android Masterclass"
//    var myFloat2: Float = 13.37F
//    val piConstant: Double = 3.14159265358979
//    var newByte: Byte = 25
//    var newShort: Short = 2020
//    var newLong: Long = 18881234567
//    var newBoolean: Boolean = true
//    var newChar: Char = 'a'

//    val var1 = 5
//    val var2 = 3
//    println("is5GreaterThan3: ${var1 > var2}")
//
//    var myNum = 5
//    myNum += 3
//    println("myNum is $myNum after +=")//prints 8
//    myNum *= 4
//    println("myNum is $myNum after *=")//prints 32
//
//    //increment & decrement
//    myNum++
//    println("myNum is $myNum after right++")//prints 33
//    println("myNum is ${myNum++} after string right++")//prints 33. increments @ stat end.
//    println("myNum is ${++myNum} after string left++")//prints 35. increments @ stat begin.
//    println("myNum is ${--myNum} after string left--")//prints 34. decrements @ stat begin.
//
//    //when expression.
//    var season = 3
//    when(season){
//        1 -> println("spring")
//        2 -> println("summer")
//        3 -> {
//            println("fall")
//            println("autumn")
//        }
//        4 -> println("winter")
//        else -> println("invalid season")
//    }
//    var month = 1
//    when(month){
//        in 3..5 -> println("spring")
//        in 6..8 -> println("summer")
//        in 9..11 -> println("fall")
//        //12, 1, 2 -> println("winter") // in 12.downTo (2) -> println("winter") doesn't work.
//        in 12.downTo(2) -> println("winter")
//        else -> println("invalid season")
//    }
//
//    var age = 19
//    when(age){
//        in 21..150 -> println("now you may drink in the us")
//        in 18..20 -> println("you may vote now")
//        16, 17 -> println("you may drive now")
//        else -> println("you're too young")
//    }

//    var feltTemp = "cold"
//    var roomTemp = 10
//    while (feltTemp == "cold"){
//        roomTemp++
//        if(roomTemp == 20){
//            feltTemp = "comfy"
//            println("it's comfy now")
//        }
//    }
//    //for loop.
//    //print("For loop example: ")
//    for(num in 1..10){
//        //print("$num")
//    }
//    for(i in 1 until 10){
//        //print("$i ")
//    }
//    //println("___________________")
//    for(i in 10 downTo 1){
//        //print("$i ")
//    }
//    //println("___________________")
//    for(i in 10 downTo 1 step 2){
//        //print("$i ")
//    }
//    //println("===================")
//    for(i in 10.downTo(1).step(2)){
//        //print("$i ")
//    }
//    //println("Integer range in descending order:")
//    // creating integer range
//    for(num in 5.downTo(1)){
//        //println(num)
//    }

    //exercise a:
//    println("___________________")
//    for(k in 0..10000){
//        if(k == 9001){
//            println("It's over 9000")
//        }
//    }
    //exercise b:
//    println("___________________")
//    var humidityLevel = 80
//    var humidity = "humid"
//
//    do {
//            if (humidity == "humid") {
//                println("$humidityLevel: humidity decreased")
//                if (humidityLevel > 60) {
//                    humidityLevel -= 5
//                } else {
//                    humidityLevel -= 1
//                }
//            }
//            if (humidityLevel < 60) {
//                println("$humidityLevel: it's comfy now")
//                humidity = "comfy"
//                humidityLevel -= 1
//            }
//    } while(humidityLevel >= 0)

    //break & continue
//    for(i in 1 until 20){ //until doesn't include the boundary.
//        print("$i")
//        if(i/2 == 5){
//            break
//        }
//    }
//    print("\ndone with the loop")

//    for(i in 1 until 20){ //until doesn't include the boundary.
//        if(i/2 == 5){
//            continue
//        }
//        print("$i")
//    }
//    print("\ndone with the loop")

    //functions.
//    println(addUp(5, 3))

//    //nullable assign.
//    var nullableName: String? = "me"
//    nullableName = null
//    //way to work with a nullable variable.
//    var len = nullableName?.length
//    nullableName = "any"
//    println(nullableName?.toUpperCase())
//    nullableName?.let {println(it.length)} //safe call operator let executes only if !null.

//    //elvis operator
//    var nullableName2: String? = null
//    val name = nullableName2 ?: "Guest" //only if nullableName2 = null a value is assigned.
//    println("name is $name")

//    //not null assertion (!! operator)
//    println(nullableName2!!.toLowerCase()) //throws a nep exception.
//
//    //ex:
////    val wifesAge: String? = user?.wife?.age ?: 0 //this is valid.

//    //class instantiation
//    var orlando = Person("Orlando", "Arias")
//    var deafult = Person() //it takes the defaults.
//    var johnPeterson = Person(lastName =  "Peterson") //it takes only one of the defaults.

////class exercise:
//var SamsungGalaxyS20 = MobilePhone("android", "samsung", "GalaxyS20Ultra")
//var Motorola = MobilePhone("android", "motorola", "moto g 5g")
//var appleiPhone = MobilePhone("ios", "apple", "14 pro Max")

    //shadowing and scope
//    myFunction(5)
    var myCar = Car()
    myCar.owner
    println("brand is : ${myCar.myBrand}")
    myCar.maxSpeed = 200
    println("brand is : ${myCar.maxSpeed}")
    //myCar.myModel = "m3" //error because setter is private.
    println("model is : ${myCar.myModel}") //np because ony the setter ir private, not the getter.
}//end of fun main()

//*********************************************


//function definition,
//fun addUp(a: Int, b: Int) : Int{
//    return a + b
//}

//classes:
class Person(firstName: String = "John", lastName: String = "Doe") {//with defaults.
    //initializer block
    init{
        println("initialized a new person object with: " + "firstName: $firstName and lastName = $lastName")
    }
}

//class exercise:
class MobilePhone(var osName: String, var brand: String, var model: String) {
    //initializer block
    init{
        println("operating system: $osName" + " brand: $brand " + " model: $model")
    }
}

//shadowing and scope
fun myFunction(aa: Int){//aa is a parameter
    var aa = 4 //aa is a variable.
    println("aa is $aa")
}

class Car() {
    lateinit var owner: String
    val myBrand: String = "bmw"
    get() {
        return field.lowercase()
    }
    init {
        this.owner = "frank"
    }

    var maxSpeed: Int = 250
//        get() = field //redundant getter
//        set(value){ //redundant setter
//            field = value
//        }
    var myModel = "m5"
        private set //private is valid only for setter
    init{
        this.myModel = "m3"
    }
}