package com.example.timersdemo

import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.timersdemo.databinding.ActivityMainBinding
class MainActivity : AppCompatActivity() {
    private var binding: ActivityMainBinding? = null
    //variable for Timer which will be initialized later.
    private var countDowntimer: CountDownTimer? = null
    //the duration pf the timer in milliseconds.
    private var timerDuration: Long = 60000
    //pauseOffset = timeDuration - time left.
    private var pauseOffset: Long = 0
    var tvTimer: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        binding = ActivityMainBinding.inflate(layoutInflater)
//        setContentView(binding?.root)
        setContentView(R.layout.activity_main)

//        toast(R.string.any_string)
        tvTimer = findViewById<TextView>(R.id.tvTimer)
        tvTimer?.text = (timerDuration/1000).toString()

        val btnStart: Button = findViewById(R.id.btnStart);
        val btnPause: Button = findViewById(R.id.btnPause);
        val btnStop: Button = findViewById(R.id.btnStop);

        btnStart.setOnClickListener {
            startTimer()
        }

        btnPause.setOnClickListener {
            pauseTimer()
        }

        btnStop.setOnClickListener {
            resetTimer()
        }
    }

    /**
     * Function is used to start the timer of 60 seconds
     */
    private fun startTimer() {

        // NOTE: if the timer is not cancelled and the start button is pressed multiple times,
        // then there is a bug that the timer will be unable to stop after that
        pauseTimer()

        /**
         * @param millisInFuture The number of millis in the future from the current time
         *  to {#start()} until the countdown is done and {#onFinish()}
         *  is called
         * @param countDownInterval The interval along the way to receive {#onTick(Long)} callbacks
         */
        countDowntimer = object : CountDownTimer(timerDuration - pauseOffset, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                pauseOffset = timerDuration - millisUntilFinished
                tvTimer?.text = (millisUntilFinished / 1000).toString()
            }

            override fun onFinish() {
//                Toast.makeText(this@MainActivity,
//                    getString(R.string.timer_is_finished), Toast.LENGTH_SHORT).show()
        toast(R.string.timer_is_finished)
            }
        }.start()
    }

    /**
     * Function is used to pause the count down timer which is running
     */
    private fun pauseTimer() {
        if (countDowntimer != null) {
            countDowntimer!!.cancel()
        }
    }

    private fun resetTimer() {
        if (countDowntimer != null) {
            countDowntimer!!.cancel()
            tvTimer?.text = (timerDuration/1000).toString()
            countDowntimer = null
            pauseOffset = 0
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        //TO avoid memory leak we disallow the binding once the activity is destroyed
        binding = null
    }
}