package com.example.kotlinbasics

fun main(){
                                                                //lecture 20
////    println("hello orlando!")
////    var myName = "orlando"
//    val myName = "orlando" //defines an immutable variable.
////    myName = "lina"  val doesn't allow overrides.
//    println("hello " + myName + "!")

                                                                //lecture 21
//    //type string
//    val myName = "orlando"
//    //type int (32 bit)
//    var myAge = 31
//
//    //integer types: Byte (8 bit), Short (16 bit), Int (32 bit), Long (64 bit).
//    val myByte: Byte = 13
//    val myShort: Short = 125
//    val myInt: Int = 123123123
//    val myLong: Long = 39_812_309_487_120_300 //the underscore is for readability.
//
//    //floating point number types: Float (32 bit), Double (64 bit).
//    val myFloat: Float = 13.37F
//    val myDouble: Double = 3.1415926535897979323846

                                                            //lecture 22
//    // Booleans the type Boolean is used to represent logical values.
//    // It can have two possible values true and false.
//    var isSunny: Boolean = true
//    // not sunny anymore...
//    isSunny = false
//
//    // Characters
//    val letterChar = 'A'
//    val digitChar = '1'
//
//    // Strings
//    val myStr = "Hello World"
//    var firCharInStr = myStr[0]
//    var lastCharInStr = myStr[myStr.length - 1]
//    println("first character " + firCharInStr)
//    print("last character " + lastCharInStr)

                                                                //lecture 24 & 25 (exercise)
//    var myString: String = "Android Masterclass"
//    val myFloat: Float = 13.37F
//    val piConstant: Double =  3.14159265358979
//    var myPennies: Byte = 25
//    var currentYear: Short = 2023
//    var cubaInhabitants: Long = 18881234567
//    var isHigh: Boolean = true
//    var myChar: Char = 'a'

//string template expression or string interpolation ($ operator).  //lecture 26
//    val myStr = "Hello World"
//    var firCharInStr = myStr[0]
//    print("first character: $firCharInStr and the length of myStr is ${myStr.length}")

                                                                //lecture 27
//    //Arithmetic operators (+, -, *, /, %)
//    var result = 5+3
//    println( result)
////    result = result / 2
////    println( result)
//    // alternatively
//     result /= 2
//    println( result)
//    result = result * 5
//    println( result)
////    result = result - 1
////    println( result)
////    // alternatively
//    result -= 1
//    println( result)
//    var moduloResult = 15%2 //the reminder.
//    println( moduloResult)
//
//    //casting.
//    val a = 5.0
//    val b = 3
//    result = (a / b).toInt()
//    println( result)
//
//    var resultDouble: Double
//    resultDouble = a / b
//    println( resultDouble)
                                                                //lecture 28
//    //Comparison operators (==, !=, <, >, <=, >=)
//    val isEqual = 5==3
//    // Concatenation - adding of "Strings"
//    println("isEqual is " + isEqual)
//    val isNotEqual = 5!=5
//    // Kotlin has a feature called String Interpolation.
//    // This feature allows you to directly insert a template expression inside a String.
//    // Template expressions are tiny pieces of code that are evaluated and
//    // their results are concatenated with the original String.
//    // A template expression is prefixed with $ symbol.
//    // Following are examples of String interpolation
//    println("isNotEqual is $isNotEqual")
//
//    println("is5Greater3 ${5 > 3}")
//    println("is5GreaterEqual3 ${5 >= 3}")
//    println("is5GreaterEqual5 ${5 >= 5}")
                                                                //lecture 29
//    //Assignment operators (+=, -=, *=, /=, %=)
//    var myNum = 5
//    myNum += 3
//    println("myNum is $myNum")
//    myNum *= 4
//    println("myNum is $myNum")
//
//
//    //Increment & Decrement operators (++, --)
//    myNum++
//    println("myNum is $myNum")
//    // increments after use
//    println("myNum is ${myNum++}")
//    // increments before use
//    println("myNum is ${++myNum}")
//    println("myNum is ${--myNum}")
    // Control Flows
// If Statements                                                //lecture 31
//    var heightPerson1 = 170
//    var heightPerson2 = 159
//    if(heightPerson1 > heightPerson2) {
//        println("use raw force")
//    } else if(heightPerson1 == heightPerson2){
//        println("use your power technique 1337")
//        }else{
//            println("use technique")
//        }
//    //challenge
//    var age = 17
//    if(age >= 21){
//        print("now you may drink in the US")
//    }
//    // Else If Statement - only executed if the if statement is not true
//    else if(age >= 18){
//        print("you may vote now")
//    }
//    // Else If Statement - only executed if the foregoing else if statement is not true
//    else if (age >= 16){
//        print("you may drive now")
//    }
//    // else is only executed if all of the foregoing statements weren't true
//    else{
//        print("you're too young")
//    }
                                                                //lecture 33
//    //if statement as an expression
//    //create a variable for testing all condition
//    val age = 17
//    //create a variable for drinkingAge
//    val drinkingAge = 21
//    //create a variable for votingAge
//    val votingAge = 18
//    //create a variable for drivingAge
//    val drivingAge = 16
//
//    //Assign the if statement to a variable
//    val currentAge =  if (age >=drinkingAge){
//        println("Now you may drink in the US")
//        //return the value for this block
//        drinkingAge
//    }else if(age >=votingAge){
//        println("You may vote now")
//        //return the value for this block
//        votingAge
//        }else if (age>=drivingAge){
//            println("You may drive now")
//            //return the value for this block
//            drivingAge
//            }else{
//                println("You are too young")
//                //return the value for this block
//                age
//            }
////print the age for the passing condition
//    print("current age is $currentAge")
                                                                //lecture 34
//    // Kotlin’s "when" expression is the replacement of the switch statement
//    // from other languages like C, C++, and Java.
//    // It is compact and more powerful than switch statements.
//    val season = 3
//    when(season) {
//        1 -> println("Spring")
//        2 -> println("Summer")
//        3 -> println("Fall")
//        4 -> println("Winter")
//        else -> println("Invalid Season")
//    }
//    val month = 3
//    when(month) {
//        in 3..5 -> println("Spring")
//        in 6..8 -> println("Summer")
//        in 9..11 -> println("Fall")
//        12, 1, 2 -> println("Winter")
//        else -> println("Invalid Season")
//    }
//
//    // challenge - translate the if statement with the age to a when expression.
//    val age = 15
//    when(age){
//        // with the !in it's the same as saying not in ...
//        !in 0..20  -> print("now you may drink in the US")
//        in 18..20  -> print("now you may vote")
//        16,17 -> print("you now may drive")
//        else -> println("you're too young")
//    }
//
//    val x : Any = 13.37f
//    when(x) {
//        is Int -> println("$x is an Int")
//        is Double -> println("$x is Double")
//        is String -> println("$x is a String")
//        else -> println("$x is none of the above")
//    }
                                                                //lecture 36
//    //when as an expression.
//    val x : Any = 13.37
//    //assign when to a variable
//    val result =  when(x) {
//    //let condition for each block be only a string
//        is Int -> "is an Int"
//        is Double -> "is Double"
//        is String -> "is a String"
//        else -> "is none of the above"
//    }
//    //then print x with the result
//    print("$x $result")
                                                                //lecture 37
//    // Loops
//    // While Loop
//    // While loop executes a block of code repeatedly as long as a given condition is true.
//
//    var y = 1
//    while(y <= 10) {
//        println("$y ")
//        y++
//    }
//
//    //challenge: starting in 100, print numbers in descending order skipping.
//    var k = 100
//    while(k >= 0) {
//        println("$k ")
//        k -= 2
//    }
                                                                //lecture 38
//    // Do while loop
//    // The do-while loop is similar to while loop except that it
//    // tests the condition at the end of the loop.
//    // This means that it will at least execute the body once
//    var z = 1
//    do {
//        print("$z ")
//        z++
//    } while(z <= 10)
                                                                //lecture 39
//    var feltTemp = "cold"
//    var roomTemp = 10
//    while (feltTemp == "cold"){
//        roomTemp++
//        if(roomTemp >= 20){
//            feltTemp = "comfy"
//            println("it's comfy now")
//        }
//    }
                                                                //lecture 40
    // For Loop
    // A for-loop is used to iterate through ranges, arrays, collections, or anything
    // that provides an iterator (You’ll learn about iterator, arrays, ranges and collections in a future lecture).
//    for(num in 1..10) {
//        print("$num ")
//    }

    // infix notation
//    for(i in 1 until 10) { // Same as - for(i in 1.until(10))
//        print("$i ")
//    }

//    for(i in 1.until(10)) {
//        print("$i ")
//    }

//    for(i in 10 downTo 1) {     // Same as - for(i in 10.downTo(1))
//        print("$i ")
//    }

//    for(i in 1 until 10 step 2) { // Same as - for(i in 1.until(10).step(2))
//        print("$i ")
//    }
                                                                //lecture 41
    //assignments:
    //Write a for loop that runs from 0 to 10000
    //Once it's at 9001 it should write "IT'S OVER 9000!!!"
//    var k = 0
//    while(k <= 10000) {
//        if(k >= 9001){
//            println("IT'S OVER 9000!!! $k")
//        }
//        k += 1
//    }

//    Write a while loop that checks the humidity (not the humidityLevel).
//    The variable humidityLevel starts at 80.
//    The variable humidity is initialized with "humid".
//    If it is "humid" then it should reduce the "humidityLevel" by 5
//    and print "humidity decreased"
//    Once the humidityLevel is below 60 it should print "it's comfy now"
//    and set the humidity to "comfy"

//    var humidityLevel = 80
//    var humidity = "humid"
//    while (humidity == "humid") {
//        humidityLevel -= 5
//        println("humidity decreased $humidityLevel")
//        if (humidityLevel < 60) {
//            humidity = "comfy"
//            println("it's comfy now $humidityLevel")
//        }
//    }
                                                                //lecture 44
    //breaks & continue.
//    for(i in 1 until 20){
//        print("$i ")
//        if(i/2 == 5){
//            break
//        }
//    }
//    print("Done with the loop")

//    for(i in 1 until 20){
//        if(i/2 == 5){
//            continue
//        }
//        print("$i ")
//    }
//    print("Done with the loop")
                                                                //lecture 45
//    val result = printUser("orlando") //this proves that a function ALWAYS returns something.
//    print(result is Unit) // Prints: true
//    myFunction()

//    println(addUp(5,3))
                                        // "a method is a function inside of a class."
//    println(avg(3.2,5.3))
                                                                //lecture 47
    // NULLABLES/OPTIONALS in Kotlin
// Kotlin supports nullability as part of its type System.
// That means You have the ability to declare whether
// a variable can hold a null value or not.
// By supporting nullability in the type system,
// the compiler can detect
// possible NullPointerException errors at compile time
// and reduce the possibility of having them thrown at runtime.

//    var name: String = "orlando"
//    name = null // Compilation Error

//    var nullableName: String? = "orlando"
//    nullableName = null // Works

// Here name cannot/must not be null
//    val len = name.length
//    val upper = name.lowercase()

// but the same methods won't work on nullable types
//    val len2 = nullableName.length // Compilation Error
//    val upper2 = nullableName.lowercase()  // Compilation Error

// So how can we solve this? We could do a null check before hand

//    val nullableName2: String? = "orlando"
//
//    if(nullableName2 != null) {
//        println("Hello, ${nullableName2.lowercase()}.")
//        println("Your name is ${nullableName2.length} characters long.")
//    } else {
//        println("Hello, Guest")
//    }

// This works but seems to be quite some work...
// So how about we shorten the syntax...
// Kotlin provides a Safe call operator, ?.
// It allows you to combine a null-check and
// a method call in a single expression.

//    nullableName2?.lowercase()

// This is the same as:
//    if(nullableName2 != null)
//        nullableName2.lowercase()
//    else
//        null

    // You can use methods on a nullable variable like this
//    val nullableName3: String? = null
//
//    println(nullableName3?.lowercase()) // prints null
//    println(nullableName3?.length) // prints null

    // Let'S say we don’t want to print anything if
// the variable is null?

// In order to perform an operation only if the
// variable is not null, we can use the safe call
// operator with let -

//    val nullableName4: String? = null
//
//    nullableName4?.let { println(it.lowercase()) }
//    nullableName4?.let { println(it.length) }
// Prints nothing because there nullableName is null
// and we used let to prevent anything from being performed
    //more on let:
//    var str = "hello kotlin let"
//    str.let{println("$it!!")}
//    println(str)
//    var strLength = str.let{"it function".length}
//    println("strLength is: $strLength")

//    var str = "Hello World"
//    str.let { println("$it!!") }
//    println(str)
//    var strLength = str.let { "$it function".length}
//    println("strLength is $strLength") //prints strLength is 20

//    var a = 1
//    var b= 2
//    a = a.let { it + 2 }
//    println(a) //prints 3

//    var a = 1
//    var b= 2
//    a = a.let { it + 2 }
//         .let { val i = it + b
//                i
//              }
//    println(a) //prints 5

                                                                //lecture 48
    //elvis operator (?:)  this assigns a default value
//    var nullableName: String? = "orlando"
//    val name = nullableName?: "guest"
//    println("name is $name") //prints: name is orlando

//    var nullableName: String? = "orlando"
//    nullableName = null
//    val name = nullableName?: "guest" //if nullableName is null then assign "guest"
//    println("name is $name") //prints: name is guest

    //not null asertion (!!) This converts a nullable to non nullable.
//    var nullableName: String? = "ORLANDO"
////    nullableName = null
//    println(nullableName!!.lowercase()) //syntax error if the previous statement is not commented.

    // You can perform a chain safe calls:
    //val wifesAge: String? = user?.wife?.age

// What if we would like to enter a default value?
// Then we can use the elvis operator ?:
    //val wifesAge2: String? = user?.wife?.age ?: 0
                                                                //lecture 54
//    var orlando = Person("orlando", "arias")
//    var john = Person()
//    var johnPeterson = Person(lastName = "Peterson")
            //exercise                                          //lecture 55
//    val firstPhone = MobilePhone(model = "Galaxy S20 Ultra")
//    val secondPhone = MobilePhone(model = "Galaxy 8")
//    val thirddPhone = MobilePhone(brand = "lg", model = "5")
                                                                //lecture 58
    //member variables (properties) and member functions (methods)
//    var orlando = Person("orlando", "arias")
//    orlando.stateHobby()
//    orlando.hobby = "to skateboard"
//    orlando.stateHobby()
//    var john = Person()
//    john.hobby = "play video games"
//    john.stateHobby()

    //using the secondary constructor
//    orlando = Person("orlando", "arias", 50)
//    orlando.age = 70
//    println("orlando age is ${orlando.age}")
                                                                //lecture 59
//    var myCar = Car("a", "b", 100)
//    myCar.owner
//    println("brand is: ${myCar.myBrand}")
//    myCar.myMaxSpeed = 200
//    println("myMaxspeed is: ${myCar.myMaxSpeed}")
//    myCar.maxSpeed = 300
//    println("maxspeed is: ${myCar.maxSpeed}")
////    myCar.myModel = "any" //syntax error because myCar.myModel is private set.
//    println("Model is: ${myCar.myModel}")

//extension functions and receivers:
    println("Is 2 even?: ${2.isEven()}") // true
    println("Is 3 even?: ${3.isEven()}") // false

}

fun printUser(user: Any) {
    println(user)
}
// The syntax of a function - fun stands for function
fun myFunction(){
    // The body of a function
    println("myFunction was called")
}

// This function has two parameters and returns a value of type Int
fun addUp(a: Int, b: Int): Int{
    // the return keyword indicates that this function will return the following value
    // once this function is called and executed
    return (a+b)
}

fun avg(a: Double, b: Double): Double {
    return  (a + b)/2
}

//class Person(firstName: String, lastName: String) { // or class Person constructor (firstName: String, lastName: String)
// Or even:
// whereby John and Doe will be default values
//class Person(var firstName: String = "John", var lastName: String= "Doe") {
//
//    // Initializer Block
//    init {
////        println("Person created")
////        this.firstName = _firstName
////        this.lastName = _lastName
//        println("Initialized a new Person object with firstName = $firstName and lastName = $lastName")
//    }
//}

//class exercise:
class MobilePhone(var osName: String = "android", var brand: String = "samsung", var model: String){
    init{
        println("The phone $model from $brand uses $osName as its operating system")
    }
}
//member variables (properties) and member functions (methods):
class Person(var firstName: String = "John", var lastName: String= "Doe"){
    // Member Variables (Properties) of the class
    var age: Int? = null
    var hobby: String = "watch Netflix"
    // Initializer Block
    init {
        println("Initialized a new Person object with firstName = $firstName and lastName = $lastName")
    }

    // Secondary Constructor
    constructor(firstName: String, lastName: String, age: Int): this(firstName, lastName)  {
        //        this.age = age
        this.age = if(age > 0) age else throw IllegalArgumentException("Age must be greater than zero")
        println("Initialized a new Person object with firstName = $firstName, lastName = $lastName, and age $age")
    }

    fun stateHobby(){
        println("$firstName's Hobby is: $hobby" )
    }
}

class Car(_brand: String, _model: String, _maxSpeed: Int) {

    lateinit var owner : String
    val brand: String = _brand

    val myBrand: String = "BMW"
        // Custom getter
        get() {
            return field.lowercase()
        }

    var maxSpeed: Int = _maxSpeed
        // Custom Setter
        set(value) {
            field = if(value > 0) value else throw IllegalArgumentException("_maxSpeed must be greater than zero")
        }
    var model: String = _model
    var maxSpeed99: Int = 250
    //the three next statements are internally generated by kotlin.
    // there is the default getter.
//        get() = field
//        set(value) {
//            field = value

    var myMaxSpeed: Int = maxSpeed

    var myModel: String = "M5"
        private set

    init{
        this.owner = "Frank"
        this.myModel = "M3"
    }
}

//extension functions and receivers:
fun Int.isEven() = this % 2 == 0
