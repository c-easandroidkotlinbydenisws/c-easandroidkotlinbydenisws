package com.example.happyplaces.activities

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.happyplaces.databinding.ActivityMainBinding
import com.example.happyplaces.database.DatabaseHandler
import com.example.happyplaces.models.HappyPlaceModel
import com.example.happyplaces.utilities.SwipeToDeleteCallback
import com.example.happyplaces.utilities.SwipeToEditCallback
import com.happyplaces.adapters.HappyPlacesAdapter

class MainActivity : AppCompatActivity() {
    private var binding: ActivityMainBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // This is used to align the xml view to this class
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT){
//            Log.i("android version: ", Build.VERSION.SDK_INT.toString())
//        }

        // Setting an click event for Fab Button and calling the AddHappyPlaceActivity.
        binding?.fabAddHappyPlace?.setOnClickListener {
//            toast(R.string.any_string)
            val intent = Intent(this@MainActivity, AddHappyPlaceActivity::class.java)
//            startActivity(intent) to fix an issue.
            // Passing an request code while activity is launched.
            startActivityForResult(intent, ADD_PLACE_ACTIVITY_REQUEST_CODE)
        }

        // Calling an function which have created for getting list when activity is launched.
        getHappyPlacesListFromLocalDB()
    }

    // It is called when the activity which launched with the request code and expecting a result from the launched activity.
    // Call Back method  to get the Message form other Activity
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // check if the request code is same as what is passed  here it is 'ADD_PLACE_ACTIVITY_REQUEST_CODE'
        if (requestCode == ADD_PLACE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                getHappyPlacesListFromLocalDB()
            }else{
                Log.e("Activity", "Cancelled or Back Pressed")
            }
        }
    }

    // Calling an function which have created for getting list of inserted data from local database. And the list of values are printed in the log.
    /**
     * A function to get the list of happy place from local database.
     */
    private fun getHappyPlacesListFromLocalDB() {
        val dbHandler = DatabaseHandler(this)
        val getHappyPlacesList = dbHandler.getHappyPlacesList()

        if (getHappyPlacesList.size > 0) {
//            for (i in getHappyPlacesList) {
//                Log.e("Title", i.title)
//                Log.e("Description", i.description)
//                Log.e("date", i.date)
//            }
            // Calling an function which have created for getting list of inserted data from local database
            //  and passing the list to recyclerview to populate in UI.

                binding?.rvHappyPlacesList?.visibility = View.VISIBLE
                binding?.tvNoRecordsAvailable?.visibility = View.GONE
                setupHappyPlacesRecyclerView(getHappyPlacesList)
        } else {
                binding?.rvHappyPlacesList?.visibility = View.GONE
                binding?.tvNoRecordsAvailable?.visibility = View.VISIBLE
        }
    }

    // Creating a function for setting up the recyclerview to UI.
    /**
     * A function to populate the recyclerview to the UI.
     */
    private fun setupHappyPlacesRecyclerView(happyPlacesList: ArrayList<HappyPlaceModel>) {
        binding?.rvHappyPlacesList?.layoutManager = LinearLayoutManager(this)
        binding?.rvHappyPlacesList?.setHasFixedSize(true)

        val placesAdapter = HappyPlacesAdapter(this, happyPlacesList)
        binding?.rvHappyPlacesList?.adapter = placesAdapter

        // Bind the onclickListener with adapter onClick function
        placesAdapter.setOnClickListener(object: HappyPlacesAdapter.OnClickListener {
            override fun onClick(position: Int, model: HappyPlaceModel) {
                val intent = Intent(this@MainActivity, HappyPlaceDetailActivity::class.java)

                // Pass the HappyPlaceDetails data model class to the detail activity.
                intent.putExtra(EXTRA_PLACE_DETAILS, model) // Passing the complete serializable data class to the detail activity using intent.
                startActivity(intent)
            }
        })

        // Bind the edit feature class to recyclerview
        val editSwipeHandler = object : SwipeToEditCallback(this) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                // Call the adapter function when it is swiped
                val adapter = binding?.rvHappyPlacesList?.adapter as HappyPlacesAdapter
                adapter.notifyEditItem(
                    this@MainActivity,
                    viewHolder.adapterPosition,
                    ADD_PLACE_ACTIVITY_REQUEST_CODE
                )
            }
        }
        val editItemTouchHelper = ItemTouchHelper(editSwipeHandler)
        editItemTouchHelper.attachToRecyclerView(binding?.rvHappyPlacesList)

        // Bind the delete feature class to recyclerview
        val deleteSwipeHandler = object : SwipeToDeleteCallback(this) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                // Call the adapter function when it is swiped for delete
                val adapter = binding?.rvHappyPlacesList?.adapter as HappyPlacesAdapter
                adapter.removeAt(viewHolder.adapterPosition)

                getHappyPlacesListFromLocalDB() // Gets the latest list from the local database after item being delete from it.
            }
        }
        val deleteItemTouchHelper = ItemTouchHelper(deleteSwipeHandler)
        deleteItemTouchHelper.attachToRecyclerView(binding?.rvHappyPlacesList)
    }

    override fun onDestroy() {
        super.onDestroy()
        //TO avoid memory leak we disallow the binding once the activity is destroyed
        binding = null
    }

    // Declare a static variable which we will using for notify the item is added when we will be returning back after adding.
    companion object{
        private const val ADD_PLACE_ACTIVITY_REQUEST_CODE = 1
        // Create a constant which will be used to put and get the data using intent from one activity to another.
        internal const val EXTRA_PLACE_DETAILS = "extra_place_details"
    }
}