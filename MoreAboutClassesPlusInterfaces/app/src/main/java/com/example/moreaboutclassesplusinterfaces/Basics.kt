package com.example.moreaboutclassesplusinterfaces

// DATA CLASSES

// Every Data Class in Kotlin needs to fulfill the following requirements -

// The primary constructor must have at least one parameter
// All the parameters declared in the primary constructor need to be marked as val or var.
// Data classes cannot be abstract, open, sealed or inner.

                                                                //lecture 61
//data class User(val id: Long, var name: String)
fun main() {
                                                                //lecture 61
//    val user1 = User(1, "orlando")
//    val name = user1.name
//    println(name)
//    user1.name = "Michael"
//    val user2 = User(1, "Michael")
//    println(user1.equals(user2))
//
//    //You can also use Kotlin’s Structural equality operator == to check for equality.
//    //The == operator internally calls the equals() method.
//    println(user1 == (user2))
//
//    //Dataclasses have a toString()
//    println("user details: $user1")
//
//  //Using the copy() Method
//  //Copies the customer object into a separate Object
//  //and updates the name.
//  //The advanteg is, that the existing customer object remains unchanged.
//    val updatedUser = user1.copy(name = "lina maria")
//    println(updatedUser.name)
//    println(updatedUser.id)
//    println(updatedUser)
//    println(updatedUser.component1())
//    println(updatedUser.component2())
//    val(id1, name1) = updatedUser
//    println("id = $id1, name = $name1")
                                                                //lecture 62.
    //challenge.
//    val myPhone = MobilePhone("android", "samsung", "s8")
//    myPhone.chargeBattery(myPhone.battery, 30)
    //end of challenge.
                                                                //lecture 64
    //inheritance
// Inheritance is one of the main concepts of
// Object Oriented Programming (OOP).
///It allows a class to inherit features
//(properties and methods) from another class and reuse them.
// The class that inherits the features of another
// class is called the Sub class or Child class or
// Derived class , and the class whose features are
// inherited is called the Super class or Parent class
// or Base class.
// All the classes in Kotlin inherit from a class called Any. It corresponds to the Object class in Java.
// Every class that you create in Kotlin implicitly
// inherits from Any -

//    var myCar = Car("A3", "Audi")
//    var myEcar = ElectricCar("S-Model", "Tesla", 85.0)

//    myCar.drive(200.0)
//    myEcar.drive(200.0)

//    var audiA3 = Car("a3", "audi")
//    var teslaS = ElectricCar("s-model", "tesla", 85.0)

//    var obj1: Any

//    teslaS.extendRange(200.0)
//    audiA3.drive(200.0)
//    teslaS.drive(200.0)

//another version
//    var audiA3 = Car("a3", "audi")
//    var teslaS = ElectricCar("s-model", "tesla", 85.0)
//    teslaS.chargerType = "Type2"
//
//    teslaS.extendRange(200.0)
//
//    teslaS.drive() //it confuses
//polymorphism
//    audiA3.drive(200.0)
//    teslaS.drive(200.0)

                                                                //lecture 65
//this is an example. nothing to do with the lecture.
//    var myHorse = Horse()
//    myHorse.isSick = false
//    println("canWork returns: ${myHorse.canWalk()}")

/*
https://kotlinlang.org/docs/tutorials/kotlin-for-py/inheritance.html#polymorphism

"The single-parent rule often becomes too limiting,
as you'll often find commonalities between classes in
different branches of a class hierarchy.
These commonalities can be expressed in interfaces.

An interface is essentially a contract that a
class may choose to sign; if it does, the class
is obliged to provide implementations of the properties
and functions of the interface.
However, an interface may (but typically doesn't)
provide a default implementation of some or all
of its properties and functions. If a property or
function has a default implementation,
the class may choose to override it,
but it doesn't have to.
*/

//var audiA3 = Car(200.0, "A3", "Audi")
//var teslaS = ElectricCar(240.0, "S-Model", "Tesla", 12.0)
//
//teslaS.chargeType = "type2"
////teslaS.extendRange(200.0)
//
////teslaS.drive()
//teslaS.brake()
//audiA3.brake()

//polymorphism
//audiA3.drive(200.0)
//teslaS.drive(200.0)
                                                                //lecture 66
// An abstract class cannot be instantiated
// (you cannot create objects of an abstract class).
// However, you can inherit subclasses from can them.
// The members (properties and methods) of an abstract class are non-abstract
// unless you explicitly use the abstract keyword to make them abstract.

//    val human = Human("Denis", "Russia",
//        70.0, 28.0)
//    val elephant = Elephant("Rosy", "India",
//        5400.0, 25.0)
//
//    //val mammal = Mammal("orlando", "colombia", 170.0) //syntax error: abstract class is not instantiable.
//
//    human.run()
//    elephant.run()
//
//    human.breath()
//    elephant.breath()
}
//**************************************************************************************************
//challenge. lecture 62.
//class MobilePhone(osName: String, brand: String, model: String){
//    var battery = 19
//    init {
//        println("The phone $model from $brand uses $osName as its Operating System")
//    }
//
//    fun chargeBattery(chargedBefore: Int, givenCharge: Int){
//        println("Initial charge: $chargedBefore. " +
//                "Was charged with: $givenCharge. " +
//                "Current charge: ${chargedBefore + givenCharge}")
//        battery += givenCharge
//    }
//}
//end of challenge. lecture 62.
                                                                //lecture 64
//inheritance
// Super class, parent class, Base class.
//open class Vehicle(){
//    //properties
//    //methods
//}

//Sub class, child class or Derived class of Vehicle.
//open class Car: Vehicle(){
//}

//Sub class, child class or Derived class of Car.
//class ElectricCar: Car(){
//}

// notice the keyword open. All classes in
// Kotlin are final by default (non-inheritable),
// so you need to use the open Keyword to make a
// class inheritable

//another version.
//Parent class
//open class Car(val name: String,
//               val brand: String) {
//}
//Sub class, child class or Derived class of Car.
//this Child class (initializes the parent class)
//class ElectricCar(name: String,
//                  brand: String,
//                  val batteryLife: Double) : Car(name, brand) {
//}

//another version.
//open class Car(val name: String, val brand: String) {
//    // open so it can be overriden by inhereting classes
//    open var range: Double = 0.0
//    fun extendRange(amount: Double) {
//        if(amount > 0) {
//            range += amount
//        }
//    }
//    fun drive(distance: Double){
//        println("Drove for $distance KM")
//    }
//}
//class ElectricCar(name: String, brand: String, batteryLife: Double) : Car(name, brand) {
//}

//another version:
//open class Car(val name: String, val brand: String) {
//    // open so it can be overriden by inhereting classes
//    open var range: Double = 0.0
//    fun extendRange(amount: Double) {
//        if(amount > 0) {
//            range += amount
//        }
//    }
//    open fun drive(distance: Double){
//        println("Drove for $distance KM")
//    }
//}
//class ElectricCar(name: String, brand: String, batteryLife: Double) : Car(name, brand) {
//    var chargerType = "Type1"
//    override var range = batteryLife * 6
//
//    override fun drive(distance: Double){
//        println("Drove for $distance KM on electricity")
//    }
//
//    fun drive(){
//        println("Drove for $range KM on electricity")
//    }
//}

                                                                //lecture 65
//this is an example. nothing to do with the lecture.
//interface Movable {
//    fun legsCount(): Int { return 0 }
//    fun wingsCount(): Int { return 0 }
//    fun canFly(): Boolean { return wingsCount() > 1 }
//    fun canWalk(): Boolean { return legsCount() > 1 }
//}

//class Horse : Movable {
//    var isSick = false
//    override fun legsCount() = 4
//    override fun canWalk(): Boolean {
//        if (isSick) {
//            return false
//        }
//        return super.canWalk()
//    }
    /*If we set horse.isSick = true,
    the canWalk() function will return false,
    regardless of the leg counts. A truly overriding capability.
     */

//interface Drivable{
//    val maxSpeed: Double
//    fun drive(): String
//    open fun brake(){
//        println("the drivable is braking")
//    }
//}
//
////Super class, parent class, base class of vehicle.
//// Class Car which extends the interface
//open class Car(override val maxSpeed: Double, val name: String, val brand: String): Drivable {
//    open var range: Double = 0.0
//
//    fun extendRange(amount: Double) {
//        if (amount > 0) {
//            range += amount
//        }
//    }
//
////    override fun drive(): String = "driving the interface drive" //short version
//
//    override fun drive(): String {//long version
//        return "driving the interface drive"
//    }
//
//    open fun drive(distance: Double){
//        println("drove for $distance km")
//    }
//}
//
//// In case there is no primary Constructor
//class ElectricCar(maxSpeed: Double,
//                  name: String,
//                  brand: String,
//                  batteryLife: Double) : Car(maxSpeed, name, brand) {
////this forces you to implement all properties and functions of Car class,
//    var chargeType = "Type1"
//    override var range = batteryLife * 6
//
//    override fun drive(distance: Double){
//        println("Drove for $distance KM on electricity")
//    }
//
//    override fun drive(): String{
//       return "drove for $range km on electricity"
//    }
//    override fun brake(){
//        super.brake()
//        println("brake inside of electric car")
//    }
//}

                                                                //lecture 66
//abstract class Mammal(private val name: String, private val origin: String,
//                      private val weight: Double) {   // Concrete (Non Abstract) Properties
//
//    // Abstract Property (Must be overridden by Subclasses)
//    abstract var maxSpeed: Double
//
//    // Abstract Methods (Must be implemented by Subclasses)
//    abstract fun run()
//    abstract fun breath()
//
//    // Concrete (Non Abstract) Method
//    fun displayDetails() {
//        println("Name: $name, Origin: $origin, Weight: $weight, " +
//                "Max Speed: $maxSpeed")
//    }
//}
//
//class Human(name: String, origin: String, weight: Double,
//            override var maxSpeed: Double): Mammal(name, origin, weight) {
//
//    override fun run() {
//        // Code to run
//        println("Runs on two legs")
//    }
//
//    override fun breath() {
//        // Code to breath
//        println("Breath through mouth or nose")
//    }
//}
//
//class Elephant(name: String, origin: String, weight: Double,
//               override var maxSpeed: Double): Mammal(name, origin, weight) {
//
//    override fun run() {
//        // Code to run
//        println("Runs on four legs")
//    }
//
//    override fun breath() {
//        // Code to breath
//        println("Breath through the trunk")
//    }
//}