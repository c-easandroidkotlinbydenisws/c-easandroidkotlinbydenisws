package com.example.collectionsandmore

import kotlin.math.floor

fun main(){
                                                                //lecture 65
//    collections (intro).
//    val stringList: List<String> = listOf("orlando", "lina", "diana")
//    val mixedTypeList: List<Any> = listOf("orlando", 31, 5, "lina", 99.5)
//
//    //alternative 1:
//    for(value in mixedTypeList) {
//        if(value is Int){
//            println("Integer: $value")
//        }else if(value is Double){
//            println("Double: $value with Floor value = ${floor(value)}")
//        }else if(value is String){
//            println("String: $value of length = ${value.length}")
//        }else{
//            println("Unknown Type")
//        }
//    }
//    //alternative 2:
//    for(value in mixedTypeList) {
//        when(value){
//            is Int -> println("integer: $value")
//            is Double -> println("Double: $value with Floor value = ${floor(value)}")
//            is String -> println("String: $value of length = ${value.length}")
//            else -> println("Unknown Type")
//        }
//    }
//    //smart cast.
//    val obj1: Any = "I have a dream"
//    if(obj1 !is String){
//        println("not a string")
//    }else{
//        //obj1 is automatically casted to a String in this scope.
//        println("found a string of ${obj1.length}")
//    }
//
//    //explicit (unsafe) casting using the "as" keyword (can go wrong).
//    val str1: String = obj1 as String
//    println(str1.length) //works because obj1 is a string.
//
//    val obj2: Any = 1337
////    val str2: String = obj2 as String //fails
////    println(str2.length) //fails because obj2 is an integer.
//
//    //explicit (safe) casting using the "as?" keyword.
//    val obj3: Any = 1337
//    val str3: String? = obj3 as? String //works
//    println(str3) //print null.

                                                                //lecture 71
//    arrays:
//    val numbers:IntArray = intArrayOf(1,2,3,4,5,6)
//    println(numbers)
//    better syntax:
//    val numbers = intArrayOf(1,2,3,4,5,6)
//    much better syntax:
//    val numbers = arrayOf(1,2,3,4,5,6)
//    print("initial values: ${numbers.contentToString()}")

//    for(myElement in numbers){
////        print(myElement)
////        print(" $myElement + 2")
//        print(" ${myElement + 2}")
//    }

//    print(numbers[0])
//    print("initial values: ${numbers.contentToString()}")
//    numbers[0] = 6
//    numbers[1] = 5
//    numbers[4] = 2
//    numbers[5] = 1
////    numbers[7] = 2 //got an ArrayIndexOutOfBoundsException exception
//    print("\nfinal values: ${numbers.contentToString()}")

//    val fruits = arrayOf(Fruit("apple", 2.5), Fruit("Grape", 3.5))
//    //    print(fruits.contentToString())
//
//    for(fruit in fruits){
//        print("\n${fruit.name}")
//    }
//
//
//    for(index in fruits.indices){
//        print("\n${fruits[index].name} is in index $index")
//    }
                                                                //lecture 72
//    //lists:
    val months = listOf("january", "february", "march")
    val anyTypes = listOf(1,2,3,true,false,"string" )
//    print(anyTypes.size)
//    print(months[1])

//    for(month in months){
//        println(month)
//    }

//    val additionalMonths = months.toMutableList()
//    val newMonths = arrayOf("april","may", "june")//this is a mutable list.
//    additionalMonths.addAll(newMonths)
//    print(additionalMonths)
//    additionalMonths.add("july")
//    print(additionalMonths.size)
//    print(additionalMonths)

//    val days = mutableListOf<String>("mon", "tue", "wed")
//    days.add("thu")
//    println(days)
//    days[2] = "sun"
//    days.removeAt(3)
//    println(days)
//    val removeList = mutableListOf<String>("mon", "wed")
//    days.removeAll(removeList)
//    println(days)
//    days.removeAll(days)
//    println(days)
                                                                //lecture 73
    //sets:
//    val fruits = setOf("orange", "apple", "grape", "apple", "Grape")//immutable set.
//    println(fruits) //it doesn't allow duplicates. it's case sensitivity.
//    println(fruits.size)
//    println(fruits.toSortedSet())

//    val fruits = setOf("orange", "apple", "mango", "grape", "apple", "orange")//immutable set.
//    val newFruits = fruits.toMutableSet()
//    newFruits.add("water melon")
//    newFruits.add("pear")
//    println(fruits)
//    println(newFruits)
//    println(newFruits.elementAt(4))

    //maps:
//    val daysOfTheWeek = mapOf(1 to "monday", 2 to "tuesday", 3 to "wednesday")
//    println(daysOfTheWeek)
//    for(key in daysOfTheWeek.keys){
//        println("$key is the key for: ${daysOfTheWeek[key]}")
//    }
//    val fruitsMap = mapOf("favorite" to Fruit("grape", 2.5), "ok" to Fruit("apple", 1.0))
//    println(fruitsMap)
//    val newDaysOfTheWeek = daysOfTheWeek.toMutableMap()
//    newDaysOfTheWeek[4] = "thursday"
//    newDaysOfTheWeek[5] = "friday"
//    println(newDaysOfTheWeek.toSortedMap())//sorts by key
                                                                //lecture 74
    //arrayList:
//    val myArrayList = ArrayList<String>() //creates an empty arrayList.
//    myArrayList.add("one")//add an element to the collection.
//    myArrayList.add("two")
//    println(myArrayList)
//    for(i in myArrayList){
//        println(i)
//    }
//    val myArrayList2: ArrayList<String> = ArrayList<String>(5)
//
//    val arrayList: ArrayList<String> = ArrayList<String>(5)
//    var list: MutableList<String> = mutableListOf<String>()
//
//    list.add("one")
//    list.add("two")
//    arrayList.addAll(list)
//    println(arrayList)
//
//    val itr = arrayList.iterator()
//    while(itr.hasNext()){
//        println(itr.next())
//    }
//    println("size of arrayList = ${arrayList.size}")
//
//    for(i in arrayList){
//        println(i)
//    }
//    println(arrayList.get(1))
                                                                //lecture 75
    //exercise
//    val arrayList: ArrayList<Double> = ArrayList(4)
//
//    arrayList.add(0.1)
//    arrayList.add(1.1)
//    arrayList.add(2.1)
//    arrayList.add(3.1)
//    arrayList.add(4.1)
//
//    var average: Double = 0.0
//    for(i in arrayList){
//        average += i
//    }
//    println("average: ${average/arrayList.size}")
                                                                //lecture 77
    //lambda expressions:
//    addNumbers(5, 10)
    //with lambda expressions:
//    val sum: (Int, Int) -> Int = {a: Int, b: Int -> a + b}
//    println(sum(10, 5))
//
//    val sum2: (Int, Int) -> Int = {a, b -> a + b}
//    println(sum2(10, 5))
//
//    val sum3 = {a: Int, b: Int -> println(a + b)}
//    sum3(10, 5)
}
//****************************************************************************************************
                                                                //lecture 71 & 73
//data class Fruit(val name: String, val price:Double)
                                                                //lecture 77
//fun addNumbers(a: Int, b: Int){//regular way
//    println("sum = ${a + b}")
//}

